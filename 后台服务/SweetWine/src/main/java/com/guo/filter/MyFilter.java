package com.guo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author 中华人民共和国
 * All rights Reserved,Designed By www.sy5retc.com
 * @Title: MyFilter.java
 * @package: com.guo.filter
 * @Description: TODO(解决跨域问题)
 * @author: 今天多喝热水
 * @date: 2021年4月20日 下午12:51:27
 * @version V1.0
 * @Copyright: 2021 All rights reserved
 * 	注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */

public class MyFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		//向下转型  ServletRequest 是HttpServletRequest的父类
				HttpServletRequest req=(HttpServletRequest) arg0;
				
				HttpServletResponse res=(HttpServletResponse) arg1;
				req.setCharacterEncoding("UTF-8");
				res.setContentType("text/html;charset=utf-8");
				
				res.setHeader("Access-Control-Allow-Origin", "*");
				res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
				res.setHeader("Access-Control-Max-Age", "0");
				res.setHeader("Access-Control-Allow-Headers",
						"Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With,userId,token");
				res.setHeader("Access-Control-Allow-Credentials", "true");
				res.setHeader("XDomainRequestAllowed", "1");
				
				//意思就是继续执行
				arg2.doFilter(req, res);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
