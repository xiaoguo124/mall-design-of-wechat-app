package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Goods;
import com.guo.service.GoodsService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: GoodsController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月2日 上午9:33:08
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class GoodsController {
	@Resource
	private GoodsService goodsService;
	

	@RequestMapping("/getAllGoods")
	@ResponseBody
	public PageInfo<Goods> getAllGoods(int pagenum, int pagesize){
		return goodsService.getAllGoods(pagenum, pagesize);
	}
	
	@RequestMapping("/getGoodsAndAll")
	@ResponseBody
	public PageInfo<Goods> getGoodsAndAll(int pagenum, int pagesize){
		return goodsService.getGoodsAndAll(pagenum, pagesize);
	}
	
	@RequestMapping("/getGoodsAndGoodsType")
	@ResponseBody
	public PageInfo<Goods> getGoodsAndGoodsType(int pagenum, int pagesize){
		return goodsService.getGoodsAndGoodsType(pagenum, pagesize);
	}
	
	@RequestMapping("/getGoodsAndPics")
	@ResponseBody
	public PageInfo<Goods> getGoodsAndPics(int pagenum, int pagesize){
		return goodsService.getGoodsAndPics(pagenum, pagesize);
	}
	
	@RequestMapping("/getGoodsAndGoodsDetail")
	@ResponseBody
	public PageInfo<Goods> getGoodsAndGoodsDetail(int pagenum, int pagesize){
		return goodsService.getGoodsAndGoodsDetail(pagenum, pagesize);
	}
	
	
	@RequestMapping("/AddGoods")
	@ResponseBody
	public String AddGoods(@RequestBody Goods goods) {
		String str="AddNG";
		if(goodsService.AddGoods(goods)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelGoods")
	@ResponseBody
	public String DelGoods(String goodsId) {
		String str="DelNG";
		if(goodsService.DelGoods(goodsId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpGoods")
	@ResponseBody
	public String UpGoods(@RequestBody Goods goods) {
		String str="UpNG";
		if(goodsService.UpGoods(goods)>0) {
			str="UpOK";
		}
		return str;
	}
	
	@RequestMapping("/SearchGoods")
	@ResponseBody
	public List<Goods> SearchGoods(@RequestBody Goods goods){
		return goodsService.SearchGoods(goods);
	}
	
	
	@RequestMapping("/SearchGoodsBygoodsId")
	@ResponseBody
	public List<Goods> SearchGoodsBygoodsId(HttpServletRequest request){
		String goodsId=request.getParameter("goodsId");
		return goodsService.SearchGoodsBygoodsId(goodsId);
	}
	
	@RequestMapping("/SearchGoodsBygoodsName")
	@ResponseBody
	public List<Goods> SearchGoodsBygoodsName(HttpServletRequest request){
		String name=request.getParameter("goodsName");
		return goodsService.SearchGoodsBygoodsName(name);
	}
	
	@RequestMapping("/getAllGoodsASC")
	@ResponseBody
	public List<Goods> getAllGoodsASC(){
		return goodsService.getAllGoodsASC();
	}
	
	@RequestMapping("/getAllGoodsDESC")
	@ResponseBody
	public List<Goods> getAllGoodsDESC(){
		return goodsService.getAllGoodsDESC();
	}
	
	@RequestMapping("/getAllGoodsForWX")
	@ResponseBody
	public List<Goods> getAllGoodsForWX(){
		return goodsService.getAllGoodsForWX();
	}
	
	
}
