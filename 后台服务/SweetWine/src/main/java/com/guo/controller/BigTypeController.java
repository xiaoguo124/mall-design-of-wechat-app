package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.BigType;
import com.guo.service.BigTypeService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: BigTypeController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:32:12
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */

@Controller
public class BigTypeController {
	
	@Resource
	BigTypeService bigtypeService;
	
	
	@RequestMapping("/getAllBigType")
	@ResponseBody
	public PageInfo<BigType> getAllBigType(int pagenum, int pagesize){
		return bigtypeService.getAllBigType( pagenum,  pagesize);
	}
	
	
	@RequestMapping("/AddBigType")
	@ResponseBody
	public String AddBigType(@RequestBody BigType bigtype) {
		String str="AddNG";
		if(bigtypeService.AddBigType(bigtype)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelBigType")
	@ResponseBody
	public String DelBigType(String bigtypeId) {
		String str="DelNG";
		if(bigtypeService.DelBigType(bigtypeId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpBigType")
	@ResponseBody
	public String UpBigType(@RequestBody BigType bigtype) {
		String str="UpNG";
		if(bigtypeService.UpBigType(bigtype)>0) {
			str="UpOK";
		}
		return str;
	}

	
	@RequestMapping("/SearchBigType")
	@ResponseBody
	public List<BigType> SearchBigType(@RequestBody BigType bigtype) {
		return bigtypeService.SearchBigType(bigtype);
	}
	
}
