package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.WineType;
import com.guo.service.WineTypeService;
/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: WineTypeController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:38:31
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class WineTypeController {
    @Resource
	private WineTypeService winetypeService;
	

	@RequestMapping("/getAllWineType")
	@ResponseBody
	public PageInfo<WineType> getAllWineType(int pagenum, int pagesize){
		return winetypeService.getAllWineType(pagenum, pagesize);
	}
	
	@RequestMapping("/getAllWineTypeForWX")
	@ResponseBody
	public List<WineType> getAllWineTypeForWX(){
		return winetypeService.getAllWineTypeForWX();
	}
	
	
	@RequestMapping("/AddWineType")
	@ResponseBody
	public String AddWineType(@RequestBody WineType WineType) {
		String str="AddNG";
		if(winetypeService.AddWineType(WineType)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelWineType")
	@ResponseBody
	public String DelWineType(String typeId) {
		String str="DelNG";
		if(winetypeService.DelWineType(typeId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpWineType")
	@ResponseBody
	public String UpWineType(@RequestBody WineType WineType) {
		String str="UpNG";
		if(winetypeService.UpWineType(WineType)>0) {
			str="UpOK";
		}
		return str;
	}
	
	
	@RequestMapping("/SearchWineType")
	@ResponseBody
	public List<WineType> SearchWineType(@RequestBody WineType WineType) {
		return winetypeService.SearchWineType(WineType);
	}
	
	

}
