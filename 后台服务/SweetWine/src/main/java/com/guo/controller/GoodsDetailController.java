package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.GoodsDetail;
import com.guo.service.GoodsDetailService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: GoodsController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:33:08
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class GoodsDetailController {
    
	@Resource
	private GoodsDetailService goodsdetailService;
	
	
	@RequestMapping("/getAllGoodsDetail")
	@ResponseBody
	public PageInfo<GoodsDetail> getAllGoodsDetail(int pagenum, int pagesize){
		return goodsdetailService.getAllGoodsDetail( pagenum, pagesize);
	}
	
	
	@RequestMapping("/AddGoodsDetail")
	@ResponseBody
	public String AddGoodsDetail(@RequestBody GoodsDetail goodsdetail) {
		String str="AddNG";
		if(goodsdetailService.AddGoodsDetail(goodsdetail)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelGoodsDetail")
	@ResponseBody
	public String DelGoodsDetail(String goodsdetailId) {
		String str="DelNG";
		Integer a = Integer.valueOf(goodsdetailId);
		if(goodsdetailService.DelGoodsDetail(a)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpGoodsDetail")
	@ResponseBody
	public String UpGoodsDetail(@RequestBody GoodsDetail goodsdetail) {
		String str="UpNG";
		if(goodsdetailService.AddGoodsDetail(goodsdetail)>0) {
			str="UpOK";
		}
		return str;
	}

	
	@RequestMapping("/SearchGoodsDetail")
	@ResponseBody
	public List<GoodsDetail> SearchGoodsDetail(@RequestBody GoodsDetail goodsdetail) {
		return goodsdetailService.SearchGoodsDetail(goodsdetail);
	}
	
	
	@RequestMapping("/SearchGoodsDetailBygoodsId")
	@ResponseBody
	public List<GoodsDetail> SearchGoodsDetailBygoodsId(HttpServletRequest request){
		String goodsId=request.getParameter("goodsId");
		return goodsdetailService.SearchGoodsDetailBygoodsId(goodsId);
	}

}
