package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.CountryType;
import com.guo.service.CountryTypeService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: CountryTypeController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:32:43
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class CountryTypeController {
	
	@Resource
	private CountryTypeService countrytypeService;
	

	@RequestMapping("/getAllCountryType")
	@ResponseBody
	public PageInfo<CountryType> getAllCountryType(int pagenum, int pagesize){
		return countrytypeService.getAllCountryType( pagenum,  pagesize);
	}
	
	@RequestMapping("/getAllCountryTypeForWX")
	@ResponseBody
	public List<CountryType> getAllCountryTypeForWX(){
		return countrytypeService.getAllCountryTypeForWX();
	}
	
	
	@RequestMapping("/AddCountryType")
	@ResponseBody
	public String AddCountryType(@RequestBody CountryType countryType) {
		String str="AddNG";
		if(countrytypeService.AddCountryType(countryType)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelCountryType")
	@ResponseBody
	public String DelCountryType(String typeId) {
		String str="DelNG";
		if(countrytypeService.DelCountryType(typeId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpCountryType")
	@ResponseBody
	public String UpCountryType(@RequestBody CountryType countryType) {
		String str="UpNG";
		if(countrytypeService.UpCountryType(countryType)>0) {
			str="UpOK";
		}
		return str;
	}
	
	
	@RequestMapping("/SearchCountryType")
	@ResponseBody
	public List<CountryType> SearchCountryType(@RequestBody CountryType countryType) {
		return countrytypeService.SearchCountryType(countryType);
	}
	

}
