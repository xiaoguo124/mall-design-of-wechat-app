package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guo.entity.Help;
import com.guo.service.HelpService;
/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: HelpController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月10日 下午3:13:29
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */

@Controller
public class HelpController {
	@Resource
	private HelpService helpService;
	
	
	@RequestMapping("/getAllHelp")
	@ResponseBody
	public List<Help> getAllHelp(){
		return helpService.getAllHelp();
	}
	
	
	@RequestMapping("/AddHelp")
	@ResponseBody
	public String AddHelp(@RequestBody Help help) {
		String str="AddNG";
		if(helpService.AddHelp(help)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelHelp")
	@ResponseBody
	public String DelHelp(String helpId) {
		String str="DelNG";
		if(helpService.DelHelp(helpId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpHelp")
	@ResponseBody
	public String UpHelp(@RequestBody Help help) {
		String str="UpNG";
		if(helpService.UpHelp(help)>0) {
			str="UpOK";
		}
		return str;
	}

	
	@RequestMapping("/SearchHelp")
	@ResponseBody
	public List<Help> SearchHelp(@RequestBody Help help) {
		return helpService.SearchHelp(help);
	}
}
