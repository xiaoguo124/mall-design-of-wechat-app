package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Swiper;
import com.guo.service.SwiperService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: SwiperController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:37:31
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class SwiperController {
	@Resource
	private SwiperService swiperService;
	
	
	@RequestMapping("/getAllSwiper")
	@ResponseBody
	public List<Swiper> getAllSwiper(){
		return swiperService.getAllSwiper();
	}
	
	
	@RequestMapping("/getSwiperAndGoods")
	@ResponseBody
	public PageInfo<Swiper> getSwiperAndGoods(int pagenum, int pagesize){
		return swiperService.getSwiperAndGoods(pagenum, pagesize);
	}

	
	@RequestMapping("/AddSwiper")
	@ResponseBody
	public String AddSwiper(@RequestBody Swiper swiper) {
		String str="AddNG";
		if(swiperService.AddSwiper(swiper)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelSwiper")
	@ResponseBody
	public String DelSwiper(String swiperId) {
		String str="DelNG";
		if(swiperService.DelSwiper(swiperId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpSwiper")
	@ResponseBody
	public String UpSwiper(@RequestBody Swiper swiper) {
		String str="UpNG";
		if(swiperService.UpSwiper(swiper)>0) {
			str="UpOK";
		}
		return str;
	}
	
	@RequestMapping("/SearchSwiper")
	@ResponseBody
	public List<Swiper> SearchSwiper(@RequestBody Swiper swiper){
		return swiperService.SearchSwiper(swiper);
	}


}
