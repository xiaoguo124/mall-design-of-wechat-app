package com.guo.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guo.entity.Admin;
import com.guo.service.AdminService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: AdminController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:31:49
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class AdminController {
	@Resource
	AdminService adminService;
	
	@RequestMapping("/AdminLogin")
	@ResponseBody
	public Map<String,Object> AdminLogin(@RequestBody Admin admin){
		Admin a=adminService.AdminLogin(admin);
		Map<String,Object> map=new HashMap<String,Object>();
		if(a!=null){
			map.put("flag", "LoginOK");
			map.put("admin",a);
		}else {
			map.put("flag", "LoginNG");
		}
		return map;
	}
	
	
	@RequestMapping("/AddAdmin")
	@ResponseBody
	public String AddAdmin(@RequestBody Admin admin) {
		String str="AddNG";
		if(adminService.AddAdmin(admin)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelAdmin")
	@ResponseBody
	public String DelAdmin(String adminId) {
		String str="DelNG";
		if(adminService.DelAdmin(adminId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpAdmin")
	@ResponseBody
	public String UpAdmin(@RequestBody Admin admin) {
		String str="UpNG";
		if(adminService.UpAdmin(admin)>0) {
			str="UpOK";
		}
		return str;
	}
	
	@RequestMapping("/SearchAdmin")
	@ResponseBody
	public List<Admin> SearchAdmin(@RequestBody Admin admin){
		return adminService.SearchAdmin(admin);
	}
}
