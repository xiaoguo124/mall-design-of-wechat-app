package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.User;
import com.guo.service.UserService;

import net.sf.json.JSONObject;


/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: UserController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月2日 上午9:38:15
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class UserController {
	@Resource
	private UserService userService;
	
	@RequestMapping("/getAllUser")
	@ResponseBody
	public PageInfo<User> getAllUser(int pagenum, int pagesize){
		return userService.getAllUser( pagenum,  pagesize);
	}

	@RequestMapping("/getUserAndAll")
	@ResponseBody
	public PageInfo<User> getUserAndAll(int pagenum, int pagesize){
		return userService.getUserAndAll(pagenum,  pagesize);
	}
	
	@RequestMapping("/getUserAndAddress")
	@ResponseBody
	public PageInfo<User> getUserAndAddress(int pagenum, int pagesize){
		return userService.getUserAndAddress(pagenum,  pagesize);
	}
	
	@RequestMapping("/getUserAndOrders")
	@ResponseBody
	public PageInfo<User> getUserAndOrders(int pagenum, int pagesize){
		return userService.getUserAndOrders(pagenum,  pagesize);
	}
	
	@RequestMapping("/DelUser")
	@ResponseBody
	public String DelUser(String userId) {
		String str="DelNG";
		if(userService.DelUser(userId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	@RequestMapping("/SearchUser")
	@ResponseBody
	public List<User> SearchUser(@RequestBody User user){
		return userService.SearchUser(user);
	}
	
	@RequestMapping("/SearchUserByuserId")
	@ResponseBody
	public List<User> SearchUserByuserId(HttpServletRequest request){
		String userId=request.getParameter("userId");
		return userService.SearchUserByuserId(userId);
	}
	
	@RequestMapping("/AddUser")
	@ResponseBody
	public String AddUser(HttpServletRequest request) {
		//微信小程序端，传递的对象叫user，对象里的属性与实体类User 一模一样
		String str=request.getParameter("user");
		JSONObject jsonObject=JSONObject.fromObject(str);
		User user=(User)JSONObject.toBean(jsonObject, User.class);
		String str1="AddNG";
		if(userService.AddUser(user)>0) {
			str1="AddOK";
		}
		return str1;
	}
	
	
	@RequestMapping("/UpUser")
	@ResponseBody
	public String UpUser(HttpServletRequest request) {
		String strUser=request.getParameter("user");
		JSONObject jsonObject=JSONObject.fromObject(strUser);
		User user=(User)JSONObject.toBean(jsonObject, User.class);
		
		String str="UpNG";
		if(userService.UpUser(user)>0) {
			str="UpOK";
		}
		return str;
	}
	

}
