package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.PriceType;
import com.guo.service.PriceTypeService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: PriceTypeController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:36:44
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class PriceTypeController {
    @Resource
	private PriceTypeService pricetypeService;
	

	@RequestMapping("/getAllPriceType")
	@ResponseBody
	public PageInfo<PriceType> getAllPriceType(int pagenum, int pagesize){
		return pricetypeService.getAllPriceType(pagenum, pagesize);
	}
	
	@RequestMapping("/getAllPriceTypeForWX")
	@ResponseBody
	public List<PriceType> getAllPriceTypeForWX(){
		return pricetypeService.getAllPriceTypeForWX();
	}
	
	
	@RequestMapping("/AddPriceType")
	@ResponseBody
	public String AddPriceType(@RequestBody PriceType PriceType) {
		String str="AddNG";
		if(pricetypeService.AddPriceType(PriceType)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelPriceType")
	@ResponseBody
	public String DelPriceType(String typeId) {
		String str="DelNG";
		if(pricetypeService.DelPriceType(typeId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpPriceType")
	@ResponseBody
	public String UpPriceType(@RequestBody PriceType PriceType) {
		String str="UpNG";
		if(pricetypeService.UpPriceType(PriceType)>0) {
			str="UpOK";
		}
		return str;
	}
	
	
	@RequestMapping("/SearchPriceType")
	@ResponseBody
	public List<PriceType> SearchPriceType(@RequestBody PriceType PriceType) {
		return pricetypeService.SearchPriceType(PriceType);
	}
	
	
}
