package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Pics;
import com.guo.service.PicsService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: PicsController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:36:16
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class PicsController {
    
	@Resource
	private PicsService picsService;
	
	
	@RequestMapping("/getAllPics")
	@ResponseBody
	public PageInfo<Pics> getAllPics(int pagenum, int pagesize){
		return picsService.getAllPics(pagenum, pagesize);
	}
	
	
	@RequestMapping("/AddPics")
	@ResponseBody
	public String AddPics(@RequestBody Pics pics) {
		String str="AddNG";
		if(picsService.AddPics(pics)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelPics")
	@ResponseBody
	public String DelPics(String picsId) {
		String str="DelNG";
		Integer a=Integer.valueOf(picsId);
		if(picsService.DelPics(a)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpPics")
	@ResponseBody
	public String UpPics(@RequestBody Pics pics) {
		String str="UpNG";
		if(picsService.AddPics(pics)>0) {
			str="UpOK";
		}
		return str;
	}

	
	@RequestMapping("/SearchPics")
	@ResponseBody
	public List<Pics> SearchPics(@RequestBody Pics pics) {
		return picsService.SearchPics(pics);
	}
	
	@RequestMapping("/SearchGoodsPicsBygoodsId")
	@ResponseBody
	public List<Pics> SearchGoodsPicsBygoodsId(HttpServletRequest request){
		String goodsId=request.getParameter("goodsId");
		return picsService.SearchGoodsPicsBygoodsId(goodsId);
	}
	
}
