package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.RecommendType;
import com.guo.service.RecommendTypeService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: RecommendTypeController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:37:10
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class RecommendTypeController {
    @Resource
	private RecommendTypeService recommendtypeService;
	

	@RequestMapping("/getAllRecommendType")
	@ResponseBody
	public PageInfo<RecommendType> getAllRecommendType(int pagenum, int pagesize){
		return recommendtypeService.getAllRecommendType(pagenum, pagesize);
	}
	
	@RequestMapping("/getAllRecommendTypeForWX")
	@ResponseBody
	public List<RecommendType> getAllRecommendTypeForWX(){
		return recommendtypeService.getAllRecommendTypeForWX();
	}
	
	
	@RequestMapping("/AddRecommendType")
	@ResponseBody
	public String AddRecommendType(@RequestBody RecommendType RecommendType) {
		String str="AddNG";
		if(recommendtypeService.AddRecommendType(RecommendType)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelRecommendType")
	@ResponseBody
	public String DelRecommendType(String typeId) {
		String str="DelNG";
		if(recommendtypeService.DelRecommendType(typeId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpRecommendType")
	@ResponseBody
	public String UpRecommendType(@RequestBody RecommendType RecommendType) {
		String str="UpNG";
		if(recommendtypeService.UpRecommendType(RecommendType)>0) {
			str="UpOK";
		}
		return str;
	}
	
	
	@RequestMapping("/SearchRecommendType")
	@ResponseBody
	public List<RecommendType> SearchRecommendType(@RequestBody RecommendType RecommendType) {
		return recommendtypeService.SearchRecommendType(RecommendType);
	}
	
	

}
