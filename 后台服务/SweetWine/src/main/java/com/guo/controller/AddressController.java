package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Address;
import com.guo.service.AddressService;

import net.sf.json.JSONObject;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: AddressController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:31:09
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class AddressController {
	@Resource
	private AddressService addressService;
	
	
	@RequestMapping("/getAllAddress")
	@ResponseBody
	public PageInfo<Address> getAllAddress(int pagenum, int pagesize){
		return addressService.getAllAddress(pagenum, pagesize);
	}
	
	@RequestMapping("/SearchAddress")
	@ResponseBody
	public List<Address> SearchAddress(@RequestBody Address address){
		return addressService.SearchAddress(address);
	}
	
	
	@RequestMapping("/AddAddress")
	@ResponseBody
	public String AddAddress(HttpServletRequest request) {
		//微信小程序端，传递的对象叫address，对象里的属性与实体类User 一致
		String strAddress=request.getParameter("address");
		JSONObject jsonObject=JSONObject.fromObject(strAddress);
		Address address=(Address)JSONObject.toBean(jsonObject, Address.class);
		String str="AddNG";
		if(addressService.AddAddress(address)>0) {
			str="AddOK";
		}
		return str;
	}
	
	@RequestMapping("/DelAddress")
	@ResponseBody
	public String DelAddress(HttpServletRequest request) {
		String id=request.getParameter("addressId");
		Integer addressId=Integer.valueOf(id);
		String str="DelNG";
		if(addressService.DelAddress(addressId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpAddress")
	@ResponseBody
	public String UpAddress(HttpServletRequest request) {
		String strAddress=request.getParameter("address");
		JSONObject jsonObject=JSONObject.fromObject(strAddress);
		Address address=(Address)JSONObject.toBean(jsonObject, Address.class);
		String str="UpNG";
		if(addressService.UpAddress(address)>0) {
			str="UpOK";
		}
		return str;
	}
	
	
	@RequestMapping("/SearchAddressByuserId")
	@ResponseBody
	public List<Address> SearchAddressByuserId(HttpServletRequest request){
		String userId=request.getParameter("userId");
		return addressService.SearchAddressByuserId(userId);
	}
	
	@RequestMapping("/SearchAddressByaddressId")
	@ResponseBody
	public List<Address> SearchAddressByaddressId(HttpServletRequest request){
		String id=request.getParameter("addressId");
		Integer addressId=Integer.valueOf(id);
		return addressService.SearchAddressByaddressId(addressId);
	}
	

}
