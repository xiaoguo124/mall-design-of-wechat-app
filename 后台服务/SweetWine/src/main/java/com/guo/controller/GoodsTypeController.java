package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.GoodsType;
import com.guo.service.GoodsTypeService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: GoodsTypeController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:34:33
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class GoodsTypeController {
	@Resource
	private GoodsTypeService goodstypeService;
	
	
	@RequestMapping("/getAllGoodsType")
	@ResponseBody
	public PageInfo<GoodsType> getAllGoodsType(int pagenum, int pagesize){
		return goodstypeService.getAllGoodsType( pagenum,  pagesize);
	}
	
	@RequestMapping("/getAllGoodsTypeForWX")
	@ResponseBody
	public List<GoodsType> getAllGoodsTypeForWX(){
		return goodstypeService.getAllGoodsTypeForWX();
	}
	
	
	@RequestMapping("/AddGoodsType")
	@ResponseBody
	public String AddGoodsType(@RequestBody GoodsType goodsType) {
		String str="AddNG";
		if(goodstypeService.AddGoodsType(goodsType)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelGoodsType")
	@ResponseBody
	public String DelGoodsType(String typeId) {
		String str="DelNG";
		if(goodstypeService.DelGoodsType(typeId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpGoodsType")
	@ResponseBody
	public String UpGoodsType(@RequestBody GoodsType goodsType) {
		String str="UpNG";
		if(goodstypeService.UpGoodsType(goodsType)>0) {
			str="UpOK";
		}
		return str;
	}
	
	
	@RequestMapping("/SearchGoodsType")
	@ResponseBody
	public List<GoodsType> SearchGoodsType(@RequestBody GoodsType goodsType) {
		return goodstypeService.SearchGoodsType(goodsType);
	}
	
	

}
