package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.ToolType;
import com.guo.service.ToolTypeService;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: ToolTypeController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月1日 上午9:37:54
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class ToolTypeController {
    @Resource
	private ToolTypeService tooltypeService;
	

	@RequestMapping("/getAllToolType")
	@ResponseBody
	public PageInfo<ToolType> getAllToolType(int pagenum, int pagesize){
		return tooltypeService.getAllToolType(pagenum, pagesize);
	}
	
	@RequestMapping("/getAllToolTypeForWX")
	@ResponseBody
	public List<ToolType> getAllToolTypeForWX(){
		return tooltypeService.getAllToolTypeForWX();
	}
	
	
	@RequestMapping("/AddToolType")
	@ResponseBody
	public String AddToolType(@RequestBody ToolType ToolType) {
		String str="AddNG";
		if(tooltypeService.AddToolType(ToolType)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelToolType")
	@ResponseBody
	public String DelToolType(String typeId) {
		String str="DelNG";
		if(tooltypeService.DelToolType(typeId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	
	@RequestMapping("/UpToolType")
	@ResponseBody
	public String UpToolType(@RequestBody ToolType ToolType) {
		String str="UpNG";
		if(tooltypeService.UpToolType(ToolType)>0) {
			str="UpOK";
		}
		return str;
	}
	
	
	@RequestMapping("/SearchToolType")
	@ResponseBody
	public List<ToolType> SearchToolType(@RequestBody ToolType ToolType) {
		return tooltypeService.SearchToolType(ToolType);
	}
	

}
