package com.guo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Address;
import com.guo.entity.Orders;
import com.guo.service.OrdersService;

import net.sf.json.JSONObject;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: OrdersController.java
@package: com.guo.controller
@Description: TODO(用一句话描述该文件做什么)
@author: 今天多喝热水
@date: 2021年5月2日 上午9:35:20
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
@Controller
public class OrdersController {
	@Resource
	private OrdersService ordersService;
	
	@RequestMapping("/getAllOrders")
	@ResponseBody
	public PageInfo<Orders> getAllOrders(int pagenum, int pagesize){
		return ordersService.getAllOrders(pagenum, pagesize);
	}
	
	@RequestMapping("/getOrdersAndAll")
	@ResponseBody
	public PageInfo<Orders> getOrdersAndAll(int pagenum, int pagesize){
		return ordersService.getOrdersAndAll(pagenum, pagesize);
	}
	
	@RequestMapping("/getOrdersAndUser")
	@ResponseBody
	public PageInfo<Orders> getOrdersAndUser(int pagenum, int pagesize){
		return ordersService.getOrdersAndUser(pagenum, pagesize);
	}
	
	@RequestMapping("/getOrdersAndAddress")
	@ResponseBody
	public PageInfo<Orders> getOrdersAndAddress(int pagenum, int pagesize){
		return ordersService.getOrdersAndAddress(pagenum, pagesize);
	}
	
	@RequestMapping("/getOrdersAndGoods")
	@ResponseBody
	public PageInfo<Orders> getOrdersAndGoods(int pagenum, int pagesize){
		return ordersService.getOrdersAndGoods(pagenum, pagesize);
	}
	
	
	@RequestMapping("/DelOrdersByAdmin")
	@ResponseBody
	public String DelOrders(String orderId) {
		String str="DelNG";
		if(ordersService.DelOrdersByAdmin(orderId)>0) {
			str="DelOK";
		}
		return str;
	}
	
	@RequestMapping("/UpOrders")
	@ResponseBody
	public String UpOrders(@RequestBody Orders orders) {
		String str="UpNG";
		if(ordersService.UpOrders(orders)>0) {
			str="UpOK";
		}
		return str;
	}
	
	@RequestMapping("/SearchOrders")
	@ResponseBody
	public List<Orders> SearchOrders(@RequestBody Orders orders){
		return ordersService.SearchOrders(orders);
	}
	
	
	@RequestMapping("/SearchOrdersByuserId")
	@ResponseBody
	public List<Orders> SearchOrdersByuserId(HttpServletRequest request){
		String userId=request.getParameter("userId");
		return ordersService.SearchOrdersByuserId(userId);
	}
	

	@RequestMapping("/AddOrders")
	@ResponseBody
	public String AddOrders(HttpServletRequest request) {
		//微信小程序端，传递的对象叫address，对象里的属性与实体类User 一致
		String strOrders=request.getParameter("orders");
		JSONObject jsonObject=JSONObject.fromObject(strOrders);
		Orders orders=(Orders)JSONObject.toBean(jsonObject, Orders.class);
		String str="AddNG";
		if(ordersService.AddOrders(orders)>0) {
			str="AddOK";
		}
		return str;
	}
	
	
	@RequestMapping("/DelOrdersByUser")
	@ResponseBody
	public String DelOrdersByUser(HttpServletRequest request) {
		//微信小程序端，传递的对象叫address，对象里的属性与实体类User 一致
		String strOrders=request.getParameter("orders");
		JSONObject jsonObject=JSONObject.fromObject(strOrders);
		Orders orders=(Orders)JSONObject.toBean(jsonObject, Address.class);
		String str="UserDelNG";
		if(ordersService.DelOrdersByUser(orders)>0) {
			str="UserDelOK";
		}
		return str;
	}

}
