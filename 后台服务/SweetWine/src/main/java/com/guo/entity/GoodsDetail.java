package com.guo.entity;

public class GoodsDetail {
	private Integer goodsdetailId;
	
	private String goodsdetailUrl;
	
	private String goodsId;

	public Integer getGoodsdetailId() {
		return goodsdetailId;
	}

	public void setGoodsdetailId(Integer goodsdetailId) {
		this.goodsdetailId = goodsdetailId;
	}

	public String getGoodsdetailUrl() {
		return goodsdetailUrl;
	}

	public void setGoodsdetailUrl(String goodsdetailUrl) {
		this.goodsdetailUrl = goodsdetailUrl;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	
	

}
