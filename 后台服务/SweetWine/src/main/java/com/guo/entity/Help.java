package com.guo.entity;
/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: Help.java
@package: com.guo.entity
@Description: TODO(选酒助手的实体类)
@author: 今天多喝热水
@date: 2021年5月10日 下午3:13:49
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
public class Help {
	private String helpId;
	
	private String helpName;
	
	private String helpIcon;
	
	private String helpContent;
	
	public String getHelpId() {
		return helpId;
	}

	public void setHelpId(String helpId) {
		this.helpId = helpId;
	}

	public String getHelpName() {
		return helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}


	public String getHelpIcon() {
		return helpIcon;
	}

	public void setHelpIcon(String helpIcon) {
		this.helpIcon = helpIcon;
	}

	public String getHelpContent() {
		return helpContent;
	}

	public void setHelpContent(String helpContent) {
		this.helpContent = helpContent;
	}
	
	

}
