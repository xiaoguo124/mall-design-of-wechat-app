package com.guo.entity;

public class User {
    private String userId;
    private String openid;
    private String nickname;
    private String gender;
    private String avatarurl;
    private String country;
    private String province;
    private String city;
    private Boolean haveuserinfo;
    private String createTime;
    private String updateTime;
    
    private Orders orders;
    private Address address;
    
    public Orders getOrders() {
		return orders;
	}
	public void setOrders(Orders orders) {
		this.orders = orders;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId ;
    }
    
    public String getOpenid() {
        return openid;
    }
    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }
    
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }
    
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }
    
    public String getAvatarurl() {
        return avatarurl;
    }
    public void setAvatarurl(String avatarurl) {
        this.avatarurl = avatarurl == null ? null : avatarurl.trim();
    }
    
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }
    
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }
    
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }
    
    public Boolean getHaveuserinfo() {
        return haveuserinfo;
    }
    public void setHaveuserinfo(Boolean haveuserinfo) {
        this.haveuserinfo = haveuserinfo;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createtime) {
        this.createTime = createtime;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updatetime) {
        this.updateTime = updatetime;
    }
}