package com.guo.entity;
/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: Pics.java
@package: com.guo.entity
@Description: TODO(商品图片，商品详情页中的轮播图)
@author: 今天多喝热水
@date: 2021年4月29日 下午9:23:54
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
public class Pics {
    private Integer picsId;

    private String picsUrl;

    private String goodsId;

    public Integer getPicsId() {
        return picsId;
    }

    public void setPicsId(Integer picsId) {
        this.picsId = picsId;
    }

    public String getPicsUrl() {
        return picsUrl;
    }

    public void setPicsUrl(String picsUrl) {
        this.picsUrl = picsUrl == null ? null : picsUrl.trim();
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId == null ? null : goodsId.trim();
    }
}