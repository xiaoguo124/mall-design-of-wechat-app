package com.guo.entity;
/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: GoodsType.java
@package: com.guo.entity
@Description: TODO(商品具体类型 实体类)
@author: 今天多喝热水
@date: 2021年4月29日 下午9:22:55
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
public class GoodsType {
    private String typeId;

    private String typeName;

    private String bigtypeId;
    
    private BigType bigType;
    
    
    
    public BigType getBigType() {
		return bigType;
	}

	public void setBigType(BigType bigType) {
		this.bigType = bigType;
	}

	public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId == null ? null : typeId.trim();
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName == null ? null : typeName.trim();
    }

    public String getBigtypeId() {
        return bigtypeId;
    }

    public void setBigtypeId(String bigtypeId) {
        this.bigtypeId = bigtypeId == null ? null : bigtypeId.trim();
    }
}