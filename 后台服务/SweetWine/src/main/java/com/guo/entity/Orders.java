package com.guo.entity;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: Orders.java
@package: com.guo.entity
@Description: TODO(订单具体信息)
@author: 今天多喝热水
@date: 2021年4月29日 下午9:23:24
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
public class Orders {
    private String orderId;

    private Integer orderTotalprice;

    private String userId;

    private String consignee;

    private String addressId;

    private String goodsId;
    
    private Integer goodsNumber;
    
	private String orderStart;

    private String createTime;

    private String updateTime;
    
    private User user;
	private Address address;
    private Goods goods;
    
    public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}
    
    
	
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public Integer getOrderTotalprice() {
        return orderTotalprice;
    }
    public void setOrderTotalprice(Integer orderTotalprice) {
        this.orderTotalprice = orderTotalprice;
    }

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getConsignee() {
        return consignee;
    }
    public void setConsignee(String consignee) {
        this.consignee = consignee == null ? null : consignee.trim();
    }

    public String getAddressId() {
        return addressId;
    }
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId == null ? null : goodsId.trim();
    }
    
	public Integer getGoodsNumber() {
		return goodsNumber;
	}
	public void setGoodsNumber(Integer goodsNumber) {
		this.goodsNumber = goodsNumber;
	}

    public String getOrderStart() {
        return orderStart;
    }
    public void setOrderStart(String orderStart) {
        this.orderStart = orderStart == null ? null : orderStart.trim();
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createtime) {
        this.createTime = createtime;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updatetime) {
        this.updateTime = updatetime;
    }
}