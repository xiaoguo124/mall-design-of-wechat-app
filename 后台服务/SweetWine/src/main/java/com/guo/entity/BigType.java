package com.guo.entity;
/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: Bigtype.java
@package: com.guo.entity
@Description: TODO(酒的类型 实体类)
@author: 今天多喝热水
@date: 2021年4月29日 下午9:20:18
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */

public class BigType {
    private String bigtypeId;

    private String bigtypeName;

    public String getBigtypeId() {
        return bigtypeId;
    }

    public void setBigtypeId(String bigtypeId) {
        this.bigtypeId = bigtypeId;
    }

    public String getBigtypeName() {
        return bigtypeName;
    }

    public void setBigtypeName(String bigtypeName) {
        this.bigtypeName = bigtypeName;
    }
}