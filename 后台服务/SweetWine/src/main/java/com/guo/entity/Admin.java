package com.guo.entity;

public class Admin {
    private String adminId;

    private String adminName;

    private String adminPwd;
    
    private String adminSex;
    
    private String adminBirthday;

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getAdminPwd() {
		return adminPwd;
	}

	public void setAdminPwd(String adminPwd) {
		this.adminPwd = adminPwd;
	}

	public String getAdminSex() {
		return adminSex;
	}

	public void setAdminSex(String adminSex) {
		this.adminSex = adminSex;
	}

	public String getAdminBirthday() {
		return adminBirthday;
	}

	public void setAdminBirthday(String adminBirthday) {
		this.adminBirthday = adminBirthday;
	}
}