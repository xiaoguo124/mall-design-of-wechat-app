package com.guo.entity;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: Swiper.java
@package: com.guo.entity
@Description: TODO(首页轮播图)
@author: 今天多喝热水
@date: 2021年4月29日 下午9:28:17
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
public class Swiper {
    private String swiperId;
    
    private String swiperUrl;
    
    private String goodsId;
    
    private Goods goods;
    
    public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public String getSwiperId() {
        return swiperId;
    }

    public void setSwiperId(String swiperId) {
        this.swiperId = swiperId == null ? null : swiperId.trim();
    }
    
    public String getSwiperUrl() {
        return swiperUrl;
    }

    public void setSwiperUrl(String swiperUrl) {
        this.swiperUrl = swiperUrl == null ? null : swiperUrl.trim();
    }
    
    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId == null ? null : goodsId.trim();
    }
}