package com.guo.entity;

/**
 * 
All rights Reserved,Designed By www.sy5retc.com
@Title: Goods.java
@package: com.guo.entity
@Description: TODO(商品信息 实体类)
@author: 今天多喝热水
@date: 2021年4月29日 下午9:22:15
@version V1.0
@Copyright: 2021 All rights reserved
注意：本内容仅限于学习用途，禁止外泄以及用于其他商业目的
 */
public class Goods {
    private String goodsId;

    private String typeId;

    private String goodsName;

    private Integer goodsPrice;

    private Integer repertory	;
    
    private String goodsIcon;
    
    private String createTime;

    private String updateTime;
    
    private Pics pics;
    private GoodsDetail goodsDetail;
    private GoodsType goodsType;
    private BigType bigType;
    
    
	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public Integer getGoodsPrice() {
		return goodsPrice;
	}
	public void setGoodsPrice(Integer goodsPrice) {
		this.goodsPrice = goodsPrice;
	}
	public Integer getRepertory() {
		return repertory;
	}
	public void setRepertoryr(Integer repertory) {
		this.repertory = repertory;
	}
	public String getGoodsIcon() {
		return goodsIcon;
	}
	public void setGoodsIcon(String goodsIcon) {
		this.goodsIcon = goodsIcon;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	
	public Pics getPics() {
		return pics;
	}
	public void setPics(Pics pics) {
		this.pics = pics;
	}
	public GoodsDetail getGoodsDetail() {
		return goodsDetail;
	}
	public void setGoodsDetail(GoodsDetail goodsDetail) {
		this.goodsDetail = goodsDetail;
	}
	public GoodsType getGoodsType() {
		return goodsType;
	}
	public void setGoodsType(GoodsType goodsType) {
		this.goodsType = goodsType;
	}
	public BigType getBigType() {
		return bigType;
	}
	public void setBigType(BigType bigType) {
		this.bigType = bigType;
	}

       
}