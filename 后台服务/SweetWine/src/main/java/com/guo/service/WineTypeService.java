package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.WineType;

public interface WineTypeService {
	public PageInfo<WineType> getAllWineType(int pagenum, int papesize);
	
	public int AddWineType(WineType wineType);
	
	public int DelWineType(String typeId);
	
	public int UpWineType(WineType wineType);
	
	public List<WineType> SearchWineType(WineType wineType);
	
	public List<WineType> getAllWineTypeForWX();
}
