package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.BigType;

public interface BigTypeService {
	public PageInfo<BigType> getAllBigType(int pagenum, int papesize);
	
	public int AddBigType(BigType bigtype);
	
	public int DelBigType(String bigtypeId);
	
	public int UpBigType(BigType bigtype);
	
	public List<BigType> SearchBigType(BigType bigtype);
	
}
