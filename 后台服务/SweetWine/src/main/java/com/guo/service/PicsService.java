package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Pics;

public interface PicsService {

    public PageInfo<Pics> getAllPics(int pagenum, int papesize);
	
	public int AddPics(Pics pics);
	
	public int DelPics(Integer picsId);
	
	public int UpPics(Pics pics);
	
	public List<Pics> SearchPics(Pics pics);
	
	public List<Pics> SearchGoodsPicsBygoodsId(String goodsId);

}
