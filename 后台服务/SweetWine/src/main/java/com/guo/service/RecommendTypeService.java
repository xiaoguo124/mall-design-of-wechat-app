package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.RecommendType;

public interface RecommendTypeService {

	public PageInfo<RecommendType> getAllRecommendType(int pagenum, int papesize);
	
	public int AddRecommendType(RecommendType recommendType);
	
	public int DelRecommendType(String typeId);
	
	public int UpRecommendType(RecommendType recommendType);
	
	public List<RecommendType> SearchRecommendType(RecommendType recommendType);
	
	public List<RecommendType> getAllRecommendTypeForWX();

}
