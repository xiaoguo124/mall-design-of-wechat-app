package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.RecommendType;
import com.guo.mapper.RecommendTypeMapper;
import com.guo.service.RecommendTypeService;

@Service
public class RecommendTypeServiceImp implements RecommendTypeService{
	@Resource
	private RecommendTypeMapper recommendtypeMapper;

	@Override
	public PageInfo<RecommendType> getAllRecommendType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<RecommendType> recommendType= recommendtypeMapper.getAllRecommendType();
		PageInfo<RecommendType> pageinfo = new PageInfo<RecommendType>(recommendType);
		return pageinfo;
	}

	@Override
	public int AddRecommendType(RecommendType recommendType) {
		// TODO Auto-generated method stub
		return recommendtypeMapper.AddRecommendType(recommendType);
	}

	@Override
	public int DelRecommendType(String typeId) {
		// TODO Auto-generated method stub
		return recommendtypeMapper.DelRecommendType(typeId);
	}

	@Override
	public int UpRecommendType(RecommendType recommendType) {
		// TODO Auto-generated method stub
		return recommendtypeMapper.UpRecommendType(recommendType);
	}

	@Override
	public List<RecommendType> SearchRecommendType(RecommendType recommendType) {
		// TODO Auto-generated method stub
		return recommendtypeMapper.SearchRecommendType(recommendType);
	}

	@Override
	public List<RecommendType> getAllRecommendTypeForWX() {
		// TODO Auto-generated method stub
		return recommendtypeMapper.getAllRecommendTypeForWX();
	}

}
