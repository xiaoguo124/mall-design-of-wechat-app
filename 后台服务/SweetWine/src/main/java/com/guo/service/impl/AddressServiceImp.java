package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.Address;
import com.guo.mapper.AddressMapper;
import com.guo.service.AddressService;

@Service
public class AddressServiceImp implements AddressService{
	
	@Resource
	AddressMapper addressMapper;

	@Override
	public PageInfo<Address> getAllAddress(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Address> address= addressMapper.getAllAddress();
		PageInfo<Address> pageinfo = new PageInfo<Address>(address);
		return pageinfo;
	}

	@Override
	public int AddAddress(Address address) {
		// TODO Auto-generated method stub
		return addressMapper.AddAddress(address);
	}

	@Override
	public int DelAddress(Integer addressId) {
		// TODO Auto-generated method stub
		return addressMapper.DelAddress(addressId);
	}

	@Override
	public int UpAddress(Address address) {
		// TODO Auto-generated method stub
		return addressMapper.UpAddress(address);
	}

	@Override
	public List<Address> SearchAddress(Address address) {
		// TODO Auto-generated method stub
		return addressMapper.SearchAddress(address);
	}
	
	
	@Override
	public List<Address> SearchAddressByuserId(String userId) {
		return addressMapper.SearchAddressByuserId(userId);
	}

	@Override
	public List<Address> SearchAddressByaddressId(Integer addressId) {
		return addressMapper.SearchAddressByaddressId(addressId);
	}
	
}
