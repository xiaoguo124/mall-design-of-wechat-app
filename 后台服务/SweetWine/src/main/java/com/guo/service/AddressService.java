package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Address;

public interface AddressService {
	
	public PageInfo<Address> getAllAddress(int pagenum, int papesize);
	
	public int AddAddress(Address address);
	
	public int DelAddress(Integer addressId);
	
	public int UpAddress(Address address);
	
	public List<Address> SearchAddress(Address address);
	
	public List<Address> SearchAddressByuserId(String userId);
	
	public List<Address> SearchAddressByaddressId(Integer addressId);
}
