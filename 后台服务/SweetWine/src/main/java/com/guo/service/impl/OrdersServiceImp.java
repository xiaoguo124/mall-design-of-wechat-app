package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.Orders;
import com.guo.mapper.OrdersMapper;
import com.guo.service.OrdersService;

@Service
public class OrdersServiceImp implements OrdersService{
	@Resource
	private OrdersMapper ordersMapper;

	@Override
	public PageInfo<Orders> getAllOrders(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Orders> orders= ordersMapper.getAllOrders();
		PageInfo<Orders> pageinfo = new PageInfo<Orders>(orders);
		return pageinfo;
	}

	@Override
	public PageInfo<Orders> getOrdersAndAll(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Orders> orders= ordersMapper.getOrdersAndAll();
		PageInfo<Orders> pageinfo = new PageInfo<Orders>(orders);
		return pageinfo;
	}

	@Override
	public PageInfo<Orders> getOrdersAndUser(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Orders> orders= ordersMapper.getOrdersAndUser();
		PageInfo<Orders> pageinfo = new PageInfo<Orders>(orders);
		return pageinfo;
	}

	@Override
	public PageInfo<Orders> getOrdersAndAddress(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Orders> orders= ordersMapper.getOrdersAndAddress();
		PageInfo<Orders> pageinfo = new PageInfo<Orders>(orders);
		return pageinfo;
	}

	@Override
	public PageInfo<Orders> getOrdersAndGoods(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Orders> orders= ordersMapper.getOrdersAndGoods();
		PageInfo<Orders> pageinfo = new PageInfo<Orders>(orders);
		return pageinfo;
	}

	@Override
	public int AddOrders(Orders orders) {
		return ordersMapper.AddOrders(orders);
	}

	@Override
	public int DelOrdersByAdmin(String orderId) {
		return ordersMapper.DelOrdersByAdmin(orderId);
	}

	@Override
	public int UpOrders(Orders orders) {
		return ordersMapper.UpOrders(orders);
	}

	@Override
	public List<Orders> SearchOrders(Orders orders) {
		return ordersMapper.SearchOrders(orders);
	}
	
	@Override
	public List<Orders> SearchOrdersByuserId(String userId) {
		return ordersMapper.SearchOrdersByuserId(userId);
	}

	@Override
	public int DelOrdersByUser(Orders orders) {
		// TODO Auto-generated method stub
		return ordersMapper.DelOrdersByUser(orders);
	}
}
