package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.CountryType;
import com.guo.mapper.CountryTypeMapper;
import com.guo.service.CountryTypeService;

@Service
public class CountryTypeServiceImp implements CountryTypeService{
	@Resource
	private CountryTypeMapper countryMapper;
	
	@Override
	public PageInfo<CountryType> getAllCountryType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<CountryType> countrytype= countryMapper.getAllCountryType();
		PageInfo<CountryType> pageinfo = new PageInfo<CountryType>(countrytype);
		return pageinfo;
	}

	@Override
	public int AddCountryType(CountryType countryType) {
		return countryMapper.AddCountryType(countryType);
	}

	@Override
	public int DelCountryType(String typeId) {
		return countryMapper.DelCountryType(typeId);
	}

	@Override
	public int UpCountryType(CountryType countryType) {
		return countryMapper.UpCountryType(countryType);
	}

	@Override
	public List<CountryType> SearchCountryType(CountryType countryType) {
		return countryMapper.SearchCountryType(countryType);
	}

	@Override
	public List<CountryType> getAllCountryTypeForWX() {
		return countryMapper.getAllCountryTypeForWX();
	}

}
