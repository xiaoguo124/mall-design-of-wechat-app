package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Swiper;

public interface SwiperService {

	public List<Swiper> getAllSwiper();
	
	public PageInfo<Swiper> getSwiperAndGoods(int pagenum, int papesize);
	
	public int AddSwiper(Swiper swiper);
	
	public int DelSwiper(String swiperId);
	
	public int UpSwiper(Swiper swiper);
	
	public List<Swiper> SearchSwiper(Swiper swiper);
}
