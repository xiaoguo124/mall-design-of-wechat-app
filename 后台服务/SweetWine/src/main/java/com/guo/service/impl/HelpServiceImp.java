package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.guo.entity.Help;
import com.guo.mapper.HelpMapper;
import com.guo.service.HelpService;

@Service
public class HelpServiceImp implements HelpService{
	@Resource
	private HelpMapper helpMapper;
	
	@Override
	public List<Help> getAllHelp() {
		return helpMapper.getAllHelp();
	}

	@Override
	public int AddHelp(Help help) {
		return helpMapper.AddHelp(help);
	}

	@Override
	public int DelHelp(String helpId) {
		return helpMapper.DelHelp(helpId);
	}

	@Override
	public int UpHelp(Help help) {
		return helpMapper.UpHelp(help);
	}

	@Override
	public List<Help> SearchHelp(Help help) {
		return helpMapper.SearchHelp(help);
	}

}
