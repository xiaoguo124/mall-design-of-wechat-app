package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.GoodsType;
import com.guo.mapper.GoodsTypeMapper;
import com.guo.service.GoodsTypeService;

@Service
public class GoodsTypeServiceImp implements GoodsTypeService{
	@Resource
	private GoodsTypeMapper goodstypeMapper;
	
	@Override
	public PageInfo<GoodsType> getAllGoodsType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<GoodsType> goodstype= goodstypeMapper.getAllGoodsType();
		PageInfo<GoodsType> pageinfo = new PageInfo<GoodsType>(goodstype);
		return pageinfo;
	}

	@Override
	public int AddGoodsType(GoodsType goodsType) {
		return goodstypeMapper.AddGoodsType(goodsType);
	}

	@Override
	public int DelGoodsType(String typeId) {
		return goodstypeMapper.DelGoodsType(typeId);
	}

	@Override
	public int UpGoodsType(GoodsType goodsType) {
		return goodstypeMapper.UpGoodsType(goodsType);
	}

	@Override
	public List<GoodsType> SearchGoodsType(GoodsType goodsType) {
		return goodstypeMapper.SearchGoodsType(goodsType);
	}

	@Override
	public List<GoodsType> getAllGoodsTypeForWX() {
		return goodstypeMapper.getAllGoodsTypeForWX();
	}

}
