package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.Swiper;
import com.guo.mapper.SwiperMapper;
import com.guo.service.SwiperService;

@Service
public class SwiperServiceImp implements SwiperService{
	@Resource
	private SwiperMapper swiperMapper;

	@Override
	public List<Swiper> getAllSwiper() {
		return swiperMapper.getAllSwiper();
	}
	
	@Override
	public PageInfo<Swiper> getSwiperAndGoods(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Swiper> swiper= swiperMapper.getSwiperAndGoods();
		PageInfo<Swiper> pageinfo = new PageInfo<Swiper>(swiper);
		return pageinfo;
	}

	@Override
	public int AddSwiper(Swiper swiper) {
		return swiperMapper.AddSwiper(swiper);
	}

	@Override
	public int DelSwiper(String swiperId) {
		return swiperMapper.DelSwiper(swiperId);
	}

	@Override
	public int UpSwiper(Swiper swiper) {
		return swiperMapper.UpSwiper(swiper);
	}

	@Override
	public List<Swiper> SearchSwiper(Swiper swiper) {
		return swiperMapper.SearchSwiper(swiper);
	}

}
