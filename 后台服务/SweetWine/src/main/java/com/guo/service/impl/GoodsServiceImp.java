package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.Goods;
import com.guo.mapper.GoodsMapper;
import com.guo.service.GoodsService;

@Service
public class GoodsServiceImp implements GoodsService{
	@Resource
	private GoodsMapper goodsMapper;

	@Override
	public PageInfo<Goods> getAllGoods(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Goods> goods= goodsMapper.getAllGoods();
		PageInfo<Goods> pageinfo = new PageInfo<Goods>(goods);
		return pageinfo;
	}
	
	@Override
	public PageInfo<Goods> getGoodsAndAll(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Goods> goods= goodsMapper.getGoodsAndAll();
		PageInfo<Goods> pageinfo = new PageInfo<Goods>(goods);
		return pageinfo;
	}

	@Override
	public PageInfo<Goods> getGoodsAndGoodsType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Goods> goods= goodsMapper.getGoodsAndGoodsType();
		PageInfo<Goods> pageinfo = new PageInfo<Goods>(goods);
		return pageinfo;
	}

	@Override
	public PageInfo<Goods> getGoodsAndPics(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Goods> goods= goodsMapper.getGoodsAndPics();
		PageInfo<Goods> pageinfo = new PageInfo<Goods>(goods);
		return pageinfo;
	}

	@Override
	public PageInfo<Goods> getGoodsAndGoodsDetail(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Goods> goods= goodsMapper.getGoodsAndGoodsDetail();
		PageInfo<Goods> pageinfo = new PageInfo<Goods>(goods);
		return pageinfo;
	}

	@Override
	public int AddGoods(Goods goods) {
		return goodsMapper.AddGoods(goods);
	}

	@Override
	public int DelGoods(String goodsId) {
		return goodsMapper.DelGoods(goodsId);
	}

	@Override
	public int UpGoods(Goods goods) {
		return goodsMapper.UpGoods(goods);
	}

	@Override
	public List<Goods> SearchGoods(Goods goods) {
		return goodsMapper.SearchGoods(goods);
	}
	
	@Override
	public List<Goods> SearchGoodsBygoodsId(String goodsId) {
		return goodsMapper.SearchGoodsBygoodsId(goodsId);
	}

	@Override
	public List<Goods> getAllGoodsASC() {
		return goodsMapper.getAllGoodsASC();
	}

	@Override
	public List<Goods> getAllGoodsDESC() {
		return goodsMapper.getAllGoodsDESC();
	}

	@Override
	public List<Goods> SearchGoodsBygoodsName(String goodsName) {
		return goodsMapper.SearchGoodsBygoodsName(goodsName);
	}
	
	@Override
	public List<Goods> getAllGoodsForWX() {
		return  goodsMapper.getAllGoodsForWX();
	}

}
