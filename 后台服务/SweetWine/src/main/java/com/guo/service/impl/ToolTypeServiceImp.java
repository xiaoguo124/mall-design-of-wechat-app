package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.ToolType;
import com.guo.mapper.ToolTypeMapper;
import com.guo.service.ToolTypeService;

@Service
public class ToolTypeServiceImp implements ToolTypeService{
	@Resource
	private ToolTypeMapper tooltypeMapper;

	@Override
	public PageInfo<ToolType> getAllToolType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<ToolType> toolType= tooltypeMapper.getAllToolType();
		PageInfo<ToolType> pageinfo = new PageInfo<ToolType>(toolType);
		return pageinfo;
	}

	@Override
	public int AddToolType(ToolType toolType) {
		// TODO Auto-generated method stub
		return tooltypeMapper.AddToolType(toolType);
	}

	@Override
	public int DelToolType(String typeId) {
		// TODO Auto-generated method stub
		return tooltypeMapper.DelToolType(typeId);
	}

	@Override
	public int UpToolType(ToolType toolType) {
		// TODO Auto-generated method stub
		return tooltypeMapper.UpToolType(toolType);
	}

	@Override
	public List<ToolType> SearchToolType(ToolType toolType) {
		// TODO Auto-generated method stub
		return tooltypeMapper.SearchToolType(toolType);
	}

	@Override
	public List<ToolType> getAllToolTypeForWX() {
		// TODO Auto-generated method stub
		return tooltypeMapper.getAllToolTypeForWX();
	}

}
