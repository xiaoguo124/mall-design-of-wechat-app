package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Goods;

public interface GoodsService {

	public PageInfo<Goods> getAllGoods(int pagenum, int papesize);
	
	public PageInfo<Goods> getGoodsAndAll(int pagenum, int papesize);
	
	public PageInfo<Goods> getGoodsAndGoodsType(int pagenum, int papesize);
	
	public PageInfo<Goods> getGoodsAndPics(int pagenum, int papesize);
	
	public PageInfo<Goods> getGoodsAndGoodsDetail(int pagenum, int papesize);
	
	public int AddGoods(Goods goods);
	
	public int DelGoods(String goodsId);
	
	public int UpGoods(Goods goods);
	
	public List<Goods> SearchGoods(Goods goods);
	
	public List<Goods> SearchGoodsBygoodsId(String goodsId);
	
	public List<Goods> SearchGoodsBygoodsName(String goodsName);
	
	public List<Goods> getAllGoodsASC();
	
	public List<Goods> getAllGoodsDESC();
	
	public List<Goods> getAllGoodsForWX();
	
}
