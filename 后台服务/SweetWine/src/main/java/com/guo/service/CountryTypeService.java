package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.CountryType;

public interface CountryTypeService {
	public PageInfo<CountryType> getAllCountryType(int pagenum, int papesize);
	
	public int AddCountryType(CountryType countryType);
	
	public int DelCountryType(String typeId);
	
	public int UpCountryType(CountryType countryType);
	
	public List<CountryType> SearchCountryType(CountryType countryType);
	
	public List<CountryType> getAllCountryTypeForWX();
	
}
