package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.Orders;

public interface OrdersService {

	public PageInfo<Orders> getAllOrders(int pagenum, int papesize);
    
	public PageInfo<Orders> getOrdersAndAll(int pagenum, int papesize);
	
	public PageInfo<Orders> getOrdersAndUser(int pagenum, int papesize);
	
	public PageInfo<Orders> getOrdersAndAddress(int pagenum, int papesize);
	
	public PageInfo<Orders> getOrdersAndGoods(int pagenum, int papesize);
	
	public int AddOrders(Orders orders);
	
	public int DelOrdersByAdmin(String ordersId);
	
	public int UpOrders(Orders orders);
	
	public List<Orders> SearchOrders(Orders orders);
	
	public List<Orders> SearchOrdersByuserId(String userId);
	
	public int DelOrdersByUser(Orders orders);
}
