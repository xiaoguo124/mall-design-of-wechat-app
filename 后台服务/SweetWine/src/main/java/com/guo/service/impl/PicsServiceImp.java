package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.Pics;
import com.guo.mapper.PicsMapper;
import com.guo.service.PicsService;

@Service
public class PicsServiceImp implements PicsService{
	@Resource
	private PicsMapper picsMapper;

	@Override
	public PageInfo<Pics> getAllPics(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<Pics> pics= picsMapper.getAllPics();
		PageInfo<Pics> pageinfo = new PageInfo<Pics>(pics);
		return pageinfo;
	}

	@Override
	public int AddPics(Pics pics) {
		return picsMapper.AddPics(pics);
	}
	
	@Override
	public int DelPics(Integer picsId) {
		return picsMapper.DelPics(picsId);
	}

	@Override
	public int UpPics(Pics pics) {
		return picsMapper.UpPics(pics);
	}

	@Override
	public List<Pics> SearchPics(Pics pics) {
		return picsMapper.SearchPics(pics);
	}
	
	@Override
	public List<Pics> SearchGoodsPicsBygoodsId(String goodsId){
		return picsMapper.SearchGoodsPicsBygoodsId(goodsId);
	}

}
