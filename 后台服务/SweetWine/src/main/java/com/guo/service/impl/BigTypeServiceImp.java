package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.BigType;
import com.guo.mapper.BigTypeMapper;
import com.guo.service.BigTypeService;

@Service
public class BigTypeServiceImp implements BigTypeService{
	@Resource
	private BigTypeMapper bigtypeMapper;
	
	@Override
	public PageInfo<BigType> getAllBigType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<BigType> bigtype= bigtypeMapper.getAllBigType();
		PageInfo<BigType> pageinfo = new PageInfo<BigType>(bigtype);
		return pageinfo;
	}

	@Override
	public int AddBigType(BigType bigtype) {
		return bigtypeMapper.AddBigType(bigtype);
	}

	@Override
	public int DelBigType(String bigtypeId) {
		return bigtypeMapper.DelBigType(bigtypeId);
	}

	@Override
	public int UpBigType(BigType bigtype) {
		return bigtypeMapper.UpBigType(bigtype);
	}

	@Override
	public List<BigType> SearchBigType(BigType bigtype) {
		return bigtypeMapper.SearchBigType(bigtype);
	}
	
	
}
