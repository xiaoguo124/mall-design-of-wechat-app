package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.GoodsDetail;
import com.guo.mapper.GoodsDetailMapper;
import com.guo.service.GoodsDetailService;

@Service
public class GoodsDetailServiceImp implements GoodsDetailService{
	@Resource
	private GoodsDetailMapper goodsdetailMapper;
	@Override
	public PageInfo<GoodsDetail> getAllGoodsDetail(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<GoodsDetail> goodsdetail= goodsdetailMapper.getAllGoodsDetail();
		PageInfo<GoodsDetail> pageinfo = new PageInfo<GoodsDetail>(goodsdetail);
		return pageinfo;
	}

	@Override
	public int AddGoodsDetail(GoodsDetail goodsDetail) {
		return goodsdetailMapper.AddGoodsDetail(goodsDetail);
	}

	@Override
	public int DelGoodsDetail(Integer goodsDetailId) {
		return goodsdetailMapper.DelGoodsDetail(goodsDetailId);
	}

	@Override
	public int UpGoodsDetail(GoodsDetail goodsDetail) {
		return goodsdetailMapper.UpGoodsDetail(goodsDetail);
	}

	@Override
	public List<GoodsDetail> SearchGoodsDetail(GoodsDetail goodsDetail) {
		return goodsdetailMapper.SearchGoodsDetail(goodsDetail);
	}
	
	@Override
	public List<GoodsDetail> SearchGoodsDetailBygoodsId(String goodsId){
		return goodsdetailMapper.SearchGoodsDetailBygoodsId(goodsId);
	}

}
