package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.WineType;
import com.guo.mapper.WineTypeMapper;
import com.guo.service.WineTypeService;

@Service
public class WineTypeServiceImp implements WineTypeService{
	@Resource
	private WineTypeMapper winetypeMapper;

	@Override
	public PageInfo<WineType> getAllWineType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<WineType> wineType= winetypeMapper.getAllWineType();
		PageInfo<WineType> pageinfo = new PageInfo<WineType>(wineType);
		return pageinfo;
	}

	@Override
	public int AddWineType(WineType wineType) {
		// TODO Auto-generated method stub
		return winetypeMapper.AddWineType(wineType);
	}

	@Override
	public int DelWineType(String typeId) {
		// TODO Auto-generated method stub
		return winetypeMapper.DelWineType(typeId);
	}

	@Override
	public int UpWineType(WineType wineType) {
		// TODO Auto-generated method stub
		return winetypeMapper.UpWineType(wineType);
	}

	@Override
	public List<WineType> SearchWineType(WineType wineType) {
		// TODO Auto-generated method stub
		return winetypeMapper.SearchWineType(wineType);
	}

	@Override
	public List<WineType> getAllWineTypeForWX() {
		// TODO Auto-generated method stub
		return winetypeMapper.getAllWineTypeForWX();
	}

}
