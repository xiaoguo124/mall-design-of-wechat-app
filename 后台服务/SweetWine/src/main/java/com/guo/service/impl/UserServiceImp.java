package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.User;
import com.guo.mapper.UserMapper;
import com.guo.service.UserService;

@Service
public class UserServiceImp implements UserService{
	@Resource
	private UserMapper userMapper;

	@Override
	public PageInfo<User> getAllUser(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<User> user= userMapper.getAllUser();
		PageInfo<User> pageinfo = new PageInfo<User>(user);
		return pageinfo;
	}

	@Override
	public PageInfo<User> getUserAndAll(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<User> user= userMapper.getUserAndAll();
		PageInfo<User> pageinfo = new PageInfo<User>(user);
		return pageinfo;
	}

	@Override
	public PageInfo<User> getUserAndAddress(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<User> user= userMapper.getUserAndAddress();
		PageInfo<User> pageinfo = new PageInfo<User>(user);
		return pageinfo;
	}

	@Override
	public PageInfo<User> getUserAndOrders(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<User> user= userMapper.getUserAndOrders();
		PageInfo<User> pageinfo = new PageInfo<User>(user);
		return pageinfo;
	}

	@Override
	public int AddUser(User user) {
		return userMapper.AddUser(user);
	}

	@Override
	public int DelUser(String userId) {
		return userMapper.DelUser(userId);
	}

	@Override
	public int UpUser(User user) {
		return userMapper.UpUser(user);
	}

	@Override
	public List<User> SearchUser(User user) {
		return userMapper.SearchUser(user);
	}
	
	@Override
	public List<User> SearchUserByuserId(String userId) {
		return userMapper.SearchUserByuserId(userId);
	}

}
