package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.PriceType;

public interface PriceTypeService {

    public PageInfo<PriceType> getAllPriceType(int pagenum, int papesize);
	
	public int AddPriceType(PriceType pricetype);
	
	public int DelPriceType(String typeId);
	
	public int UpPriceType(PriceType pricetype);
	
	public List<PriceType> SearchPriceType(PriceType pricetype);
	
	public List<PriceType> getAllPriceTypeForWX();
	
}
