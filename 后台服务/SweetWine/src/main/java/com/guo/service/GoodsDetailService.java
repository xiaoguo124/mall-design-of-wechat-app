package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.GoodsDetail;

public interface GoodsDetailService {

    public PageInfo<GoodsDetail> getAllGoodsDetail(int pagenum, int papesize);
	
	public int AddGoodsDetail(GoodsDetail goodsDetail);
	
	public int DelGoodsDetail(Integer goodsDetailId);
	
	public int UpGoodsDetail(GoodsDetail goodsDetail);
	
	public List<GoodsDetail> SearchGoodsDetail(GoodsDetail goodsDetail);
	
	public List<GoodsDetail> SearchGoodsDetailBygoodsId(String goodsId);
}
