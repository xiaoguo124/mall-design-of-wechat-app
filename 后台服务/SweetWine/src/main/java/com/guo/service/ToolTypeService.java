package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.ToolType;

public interface ToolTypeService {
	public PageInfo<ToolType> getAllToolType(int pagenum, int papesize);
	
	public int AddToolType(ToolType toolType);
	
	public int DelToolType(String typeId);
	
	public int UpToolType(ToolType toolType);
	
	public List<ToolType> SearchToolType(ToolType toolType);
	
	public List<ToolType> getAllToolTypeForWX();
}
