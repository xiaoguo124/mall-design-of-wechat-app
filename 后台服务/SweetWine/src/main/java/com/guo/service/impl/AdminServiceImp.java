package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.guo.entity.Admin;
import com.guo.mapper.AdminMapper;
import com.guo.service.AdminService;

@Service
public class AdminServiceImp implements AdminService{
	@Resource
	AdminMapper adminMapper;
	
	@Override
	public Admin AdminLogin(Admin admin) {
		return adminMapper.AdminLogin(admin);
	}

	@Override
	public int AddAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.AddAdmin(admin);
	}

	@Override
	public int DelAdmin(String adminId) {
		// TODO Auto-generated method stub
		return adminMapper.DelAdmin(adminId);
	}

	@Override
	public int UpAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.UpAdmin(admin);
	}

	@Override
	public List<Admin> SearchAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.SearchAdmin(admin);
	}
}
