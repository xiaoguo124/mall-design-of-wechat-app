package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.GoodsType;

public interface GoodsTypeService {
	public PageInfo<GoodsType> getAllGoodsType(int pagenum, int papesize);
	
	public int AddGoodsType(GoodsType goodsType);
	
	public int DelGoodsType(String typeId);
	
	public int UpGoodsType(GoodsType goodsType);
	
	public List<GoodsType> SearchGoodsType(GoodsType goodsType);
	
	public List<GoodsType> getAllGoodsTypeForWX();
	
}
