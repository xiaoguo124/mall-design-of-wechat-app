package com.guo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.guo.entity.PriceType;
import com.guo.mapper.PriceTypeMapper;
import com.guo.service.PriceTypeService;

@Service
public class PriceTypeServiceImp implements PriceTypeService{
	@Resource
	private PriceTypeMapper pricetypeMapper;

	@Override
	public PageInfo<PriceType> getAllPriceType(int pagenum, int papesize) {
		PageHelper.startPage(pagenum, papesize);
		List<PriceType> pricetype= pricetypeMapper.getAllPriceType();
		PageInfo<PriceType> pageinfo = new PageInfo<PriceType>(pricetype);
		return pageinfo;
	}

	@Override
	public int AddPriceType(PriceType pricetype) {
		// TODO Auto-generated method stub
		return pricetypeMapper.AddPriceType(pricetype);
	}

	@Override
	public int DelPriceType(String typeId) {
		// TODO Auto-generated method stub
		return pricetypeMapper.DelPriceType(typeId);
	}

	@Override
	public int UpPriceType(PriceType pricetype) {
		// TODO Auto-generated method stub
		return pricetypeMapper.UpPriceType(pricetype);
	}

	@Override
	public List<PriceType> SearchPriceType(PriceType pricetype) {
		// TODO Auto-generated method stub
		return pricetypeMapper.SearchPriceType(pricetype);
	}

	@Override
	public List<PriceType> getAllPriceTypeForWX() {
		return pricetypeMapper.getAllPriceTypeForWX();
	}

}
