package com.guo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.guo.entity.User;

public interface UserService {

	public PageInfo<User> getAllUser(int pagenum, int papesize);
	
	public PageInfo<User> getUserAndAll(int pagenum, int papesize);
	
	public PageInfo<User> getUserAndAddress(int pagenum, int papesize);
	
	public PageInfo<User> getUserAndOrders(int pagenum, int papesize);
	
	public int AddUser(User user);
	
	public int DelUser(String userId);
	
	public int UpUser(User user);
	
	public List<User> SearchUser(User user);
	
	public List<User> SearchUserByuserId(String userId);
	
}
