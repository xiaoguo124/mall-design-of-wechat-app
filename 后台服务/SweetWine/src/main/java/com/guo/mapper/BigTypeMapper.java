package com.guo.mapper;

import java.util.List;

import com.guo.entity.BigType;

public interface BigTypeMapper {
	public List<BigType> getAllBigType();
	
	public int AddBigType(BigType bigtype);
	
	public int DelBigType(String bigtypeId);
	
	public int UpBigType(BigType bigtype);
	
	public List<BigType> SearchBigType(BigType bigtype);
    
}