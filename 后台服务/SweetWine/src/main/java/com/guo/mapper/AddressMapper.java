package com.guo.mapper;

import java.util.List;

import com.guo.entity.Address;

public interface AddressMapper {
	public List<Address> getAllAddress();
	
	public int AddAddress(Address address);
	
	public int DelAddress(Integer addressId);
	
	public int UpAddress(Address address);
	
	public List<Address> SearchAddress(Address address);
	
	public List<Address> SearchAddressByuserId(String userId);
	
	public List<Address> SearchAddressByaddressId(Integer addressId);
}