package com.guo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.guo.entity.Goods;

public interface GoodsMapper {
	
	public List<Goods> getAllGoods();
	
	public List<Goods> getGoodsAndAll();
	
	public List<Goods> getGoodsAndGoodsType();
	
	public List<Goods> getGoodsAndPics();
	
	public List<Goods> getGoodsAndGoodsDetail();
	
	public int AddGoods(Goods goods);
	
	public int DelGoods(String goodsId);
	
	public int UpGoods(Goods goods);
	
	public List<Goods> SearchGoods(Goods goods);
	
	public List<Goods> SearchGoodsBygoodsId(String goodsId);
	
	public List<Goods> getAllGoodsASC();
	
	public List<Goods> getAllGoodsDESC();
	
	public List<Goods> SearchGoodsBygoodsName(@Param("goodsName") String goodsName);
	
	public List<Goods> getAllGoodsForWX();
}