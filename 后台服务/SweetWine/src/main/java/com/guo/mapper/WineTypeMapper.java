package com.guo.mapper;

import java.util.List;

import com.guo.entity.WineType;

public interface WineTypeMapper {
    public List<WineType> getAllWineType();
	
	public int AddWineType(WineType wineType);
	
	public int DelWineType(String typeId);
	
	public int UpWineType(WineType wineType);
	
	public List<WineType> SearchWineType(WineType wineType);
	
    public List<WineType> getAllWineTypeForWX();
}