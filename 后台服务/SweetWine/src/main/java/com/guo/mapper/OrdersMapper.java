package com.guo.mapper;

import java.util.List;

import com.guo.entity.Orders;

public interface OrdersMapper {
	
	public List<Orders> getAllOrders();
    
	public List<Orders> getOrdersAndAll();
	
	public List<Orders> getOrdersAndUser();
	
	public List<Orders> getOrdersAndAddress();
	
	public List<Orders> getOrdersAndGoods();
	
	public int AddOrders(Orders orders);
	
	public int DelOrdersByAdmin(String orderId);
	
	public int UpOrders(Orders orders);
	
	public List<Orders> SearchOrders(Orders orders);
	
	public List<Orders> SearchOrdersByuserId(String userId);
	
	public int DelOrdersByUser(Orders orders);
    
}