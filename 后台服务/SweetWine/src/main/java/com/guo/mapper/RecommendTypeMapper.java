package com.guo.mapper;

import java.util.List;

import com.guo.entity.RecommendType;

public interface RecommendTypeMapper {
	
	public List<RecommendType> getAllRecommendType();
	
	public int AddRecommendType(RecommendType recommendType);
	
	public int DelRecommendType(String typeId);
	
	public int UpRecommendType(RecommendType recommendType);
	
	public List<RecommendType> SearchRecommendType(RecommendType recommendType);
	
	public List<RecommendType> getAllRecommendTypeForWX();
	
}