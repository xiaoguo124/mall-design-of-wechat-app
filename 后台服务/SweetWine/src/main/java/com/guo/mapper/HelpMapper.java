package com.guo.mapper;

import java.util.List;

import com.guo.entity.Help;

public interface HelpMapper {
	public List<Help> getAllHelp();
	
	public int AddHelp(Help help);
	
	public int DelHelp(String helpId);
	
	public int UpHelp(Help help);
	
	public List<Help> SearchHelp(Help help);

}
