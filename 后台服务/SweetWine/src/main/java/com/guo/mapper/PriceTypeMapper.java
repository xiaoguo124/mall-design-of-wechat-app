package com.guo.mapper;

import java.util.List;

import com.guo.entity.PriceType;

public interface PriceTypeMapper {
	
    public List<PriceType> getAllPriceType();
	
	public int AddPriceType(PriceType pricetype);
	
	public int DelPriceType(String typeId);
	
	public int UpPriceType(PriceType pricetype);
	
	public List<PriceType> SearchPriceType(PriceType pricetype);
	
	public List<PriceType> getAllPriceTypeForWX();
	
}