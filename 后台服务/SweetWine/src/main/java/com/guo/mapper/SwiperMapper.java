package com.guo.mapper;

import java.util.List;

import com.guo.entity.Swiper;

public interface SwiperMapper {
	
	public List<Swiper> getAllSwiper();
	
	public List<Swiper> getSwiperAndGoods();
	
	public int AddSwiper(Swiper swiper);
	
	public int DelSwiper(String swiperId);
	
	public int UpSwiper(Swiper swiper);
	
	public List<Swiper> SearchSwiper(Swiper swiper);
}