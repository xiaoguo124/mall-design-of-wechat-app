package com.guo.mapper;

import java.util.List;

import com.guo.entity.User;

public interface UserMapper {
	
	public List<User> getAllUser();
	
	public List<User> getUserAndAll();
	
	public List<User> getUserAndAddress();
	
	public List<User> getUserAndOrders();
	
	public int AddUser(User user);
	
	public int DelUser(String userId);
	
	public int UpUser(User user);
	
	public List<User> SearchUser(User user);
	
	public List<User> SearchUserByuserId(String userId);
	
}