package com.guo.mapper;

import java.util.List;

import com.guo.entity.GoodsType;

public interface GoodsTypeMapper {
	public List<GoodsType> getAllGoodsType();
	
	public int AddGoodsType(GoodsType goodsType);
	
	public int DelGoodsType(String typeId);
	
	public int UpGoodsType(GoodsType goodsType);
	
	public List<GoodsType> SearchGoodsType(GoodsType goodsType);
	
	public List<GoodsType> getAllGoodsTypeForWX();
    
}