package com.guo.mapper;

import java.util.List;

import com.guo.entity.CountryType;

public interface CountryTypeMapper {
	public List<CountryType> getAllCountryType();
	
	public int AddCountryType(CountryType countryType);
	
	public int DelCountryType(String typeId);
	
	public int UpCountryType(CountryType countryType);
	
	public List<CountryType> SearchCountryType(CountryType countryType);
	
	public List<CountryType> getAllCountryTypeForWX();
    
}