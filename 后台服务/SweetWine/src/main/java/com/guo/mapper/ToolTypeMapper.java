package com.guo.mapper;

import java.util.List;

import com.guo.entity.ToolType;

public interface ToolTypeMapper {
    public List<ToolType> getAllToolType();
	
	public int AddToolType(ToolType toolType);
	
	public int DelToolType(String typeId);
	
	public int UpToolType(ToolType toolType);
	
	public List<ToolType> SearchToolType(ToolType toolType);

    public List<ToolType> getAllToolTypeForWX();
	
}