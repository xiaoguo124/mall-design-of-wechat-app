package com.guo.mapper;

import java.util.List;

import com.guo.entity.Admin;

public interface AdminMapper {
	public Admin AdminLogin(Admin admin);
	
	public int AddAdmin(Admin admin);
	
	public int DelAdmin(String adminId);
	
	public int UpAdmin(Admin admin);
	
	public List<Admin> SearchAdmin(Admin admin);

}