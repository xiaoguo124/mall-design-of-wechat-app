package com.guo.mapper;

import java.util.List;

import com.guo.entity.Pics;

public interface PicsMapper {
	
    public List<Pics> getAllPics();
	
	public int AddPics(Pics pics);
	
	public int DelPics(Integer picsId);
	
	public int UpPics(Pics pics);
	
	public List<Pics> SearchPics(Pics pics);
	
	public List<Pics> SearchGoodsPicsBygoodsId(String goodsId);
}