package com.guo.mapper;

import java.util.List;

import com.guo.entity.GoodsDetail;

public interface GoodsDetailMapper {
	
    public List<GoodsDetail> getAllGoodsDetail();
	
	public int AddGoodsDetail(GoodsDetail goodsDetail);
	
	public int DelGoodsDetail(Integer goodsDetailId);
	
	public int UpGoodsDetail(GoodsDetail goodsDetail);
	
	public List<GoodsDetail> SearchGoodsDetail(GoodsDetail goodsDetail);
	
	
	public List<GoodsDetail> SearchGoodsDetailBygoodsId(String goodsId);
	
}