
const orderId = date =>{
    var year=date.getFullYear();
    year=year%100;
    const month=date.getMonth()+1;
    const day=date.getDate();
    const hour=date.getHours();
    const minute=date.getMinutes();
    const second=date.getSeconds();
    const haomiao=date.getMilliseconds();
    // 格式样式 210306142335321 15位
    return [year, month, day, hour, minute, second, haomiao].map(formatNumber).join('');
}


const formatTime = date =>{
    const year=date.getFullYear();
    const month=date.getMonth()+1;
    const day=date.getDate();
    const hour=date.getHours();
    const minute=date.getMinutes();
    const second=date.getSeconds();
    // 格式样式 2021-03-06
    return [year, month, day].map(formatNumber).join('-') + ' ' +[hour, minute, second].map(formatNumber).join(':');
}

const formatDate = date =>{
    const year=date.getFullYear();
    const month=date.getMonth()+1;
    const day=date.getDate();
    // 格式样式 2021/04/21
    return [year, month, day].map(formatNumber).join('/');
}

const formatNumber = n =>{
    n = n.toString();
    return n[1]?n:'0'+n;
}

module.exports = {
    orderId: orderId,
    formatDate: formatDate,
    formatTime: formatTime
}


