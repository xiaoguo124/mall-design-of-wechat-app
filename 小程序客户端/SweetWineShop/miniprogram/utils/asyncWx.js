// promise 形式 getSeting
export const getSetting=()=>{
    return new Promise((resolve,reject)=>{
        wx.getSetting({
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            }
        });
          
    })
}

// promise 形式 chooseAddress
export const chooseAddress=()=>{
    return new Promise((resolve,reject)=>{
        wx.chooseAddress({
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            }
        });
          
    })
}

// promise 形式的showModal 弹出提示框
export const showModal=({content})=>{
    return new Promise((resolve,reject)=>{
        wx.showModal({
            title: '提示',
            content: content,
            success: (res) => {
                resolve(res);
            },
            fail:(err)=>{
                reject(err);
            }
        })
    })
}


// promise 形式的showModal 弹出提示框 以下两个是支付时用来玩的
export const PayWan=({content})=>{
    return new Promise((resolve,reject)=>{
        wx.showModal({
            content: content,
            success: (res) => {
                resolve(res);
            },
            fail:(err)=>{
                reject(err);
            }
        })
    })
}
export const PayLoading=()=>{
    return new Promise((resolve,reject)=>{
        wx.showToast({
            title: '正在获取支付信息',
            icon: 'loading',
            duration: 1500,
            mask:true,
            success: (res) => {
                resolve(res);
            },
            fail:(err)=>{
                reject(err);
            }
        })
    })
}



// promise 形式的showToast 弹出提示框
export const showToast=({title})=>{
    return new Promise((resolve,reject)=>{
        wx.showToast({
            title: title,
            icon: 'none',
            success: (res) => {
                resolve(res);
            },
            fail:(err)=>{
                reject(err);
            }
        })
    })
}

// promise 形式的 login 
export const login=()=>{
    return new Promise((resolve,reject)=>{
        wx.login({
            timeout:10000,
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            }
        });
    })    
}

// promise 形式的小程序微信支付 pay是支付所必须的参数
export const requestPayment=(pay)=>{
    return new Promise((resolve,reject)=>{
        wx.requestPayment({
            ...pay,
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            }
        });
          
    })    
}

// promise 形式的小 getUserProfile 获取用户信息
export const getUserProfile=()=>{
    return new Promise((resolve,reject)=>{
        wx.getUserProfile({
            desc:"用于完善会员资料",
            success:(result)=>{
                resolve(result);
            },
            fail:(err)=>{}
        })  
    })    
}