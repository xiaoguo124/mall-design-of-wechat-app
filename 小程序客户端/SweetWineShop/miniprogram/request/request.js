// 同时发送异步请求的次数
let ajaxTimes=0;
export const request=(params)=>{
  ajaxTimes++;
  //显示‘加载中’效果
  wx.showLoading({
    title: '加载中',
    mask:true
  });
  // 定义公共的url
  const comUrl="http://192.168.43.40/SweetWine"
  return new Promise((resolve,reject)=>{
    wx.request({
      ...params,
      url:comUrl+params.url+".action",
      success: (result) => {
        resolve(result);
      },
      fail:(err)=>{
        reject(err);
      },
      complete:()=>{
        ajaxTimes--;
        if(ajaxTimes===0){
          //关闭‘加载中’图标
          wx.hideLoading();
        }
      }
    });
  })
}