// pages/AddAddress/AddAddress.js

import {request} from "../../request/request.js";
import regeneratorRuntime from "../../lib/runtime/runtime.js";
var util =require('../../utils/util.js');
import {getSetting,chooseAddress,openSetting,showModal,showToast} from "../../utils/asyncWx.js";

Page({
  data:{
    openid:"",
    consignee:"",
    userPhone:"",
    region:[],
    xiangxidizhi:"",

    // 点击定位
    dingwei:false,
    addresscity:"",

    // 点击修改 需要显示数据
    xiugai:false,
    addressId:"",
    addressInfo:{}
    
  },
  onShow(){
    this.getOpenid();
    // 修改信息 需要获取传递来的值
    let pages = getCurrentPages();
    let currentPage=pages[pages.length-1];
    let options=currentPage.options;
    const {addressId}=options;
    if(addressId!=null){
      this.setData({
        xiugai:true,
        addressId:addressId
      })
      this.SearchAddressByaddressId(addressId);
    }
  },
  
  async SearchAddressByaddressId(addressId){
    const res=await request({url:"/SearchAddressByaddressId",data:{addressId}});
    this.setData({
      addressInfo:res.data[0]
    })
  },

  Consignee: function (e) {
    this.setData({
      consignee: e.detail.value
    })
  },
  UserPhone: function (e) {
    this.setData({
      userPhone: e.detail.value
    })
  },
  Xiangxidizhi: function (e) {
    this.setData({
      xiangxidizhi: e.detail.value
    })
  },
  // 选择省市区函数
  ChooseCity: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  async BaoCun(){
    if(this.data.xiugai){
      this.UpAddress();
    }else{
      this.AddAddress()
    }
  },
  async AddAddress(){
    var TIME = util.formatTime(new Date());
    var address={
      addressName:this.data.xiangxidizhi,
      userId:this.data.openid,
      consignee:this.data.consignee,
      userPhone:this.data.userPhone,
      createTime:TIME,
      updateTime:TIME
    };
    if(this.data.dingwei){
      address.addressArea=this.data.addresscity;
    }else{
      address.addressArea=this.data.region[0]+this.data.region[1]+this.data.region[2]
    }
    if(address.addressName!="" && address.consignee!="" && address.userPhone!="" && this.data.xiangxidizhi){
      
      if ((/(^\d{11}$)/.test(address.userPhone))) {
        request({url:"/AddAddress",data:{address}});
        wx.showToast({
          title: '添加成功',
          icon: 'success',
          mask: true
        }); 
        wx.navigateBack({
          delta: 1
        });
      }else{
        showModal({content:"请输入正确的手机号码"});
      }
    }else{
      //弹窗提示
      showModal({content:"请输入完整信息"});
      /*只是给个提示框 点取消还是确定都不做处理
      if(res.confirm){
        console.log('弹框后点确定')
      }else{
        console.log('弹框后点取消')
      }
      */
    }
  },
  async UpAddress(){
    var TIME = util.formatTime(new Date());
    var address={
      addressId:this.data.addressId,
      userId:this.data.openid,
      addressArea:this.data.region[0]+this.data.region[1]+this.data.region[2],
      addressName:this.data.xiangxidizhi,
      consignee:this.data.consignee,
      userPhone:this.data.userPhone,
      updateTime:TIME
    };
    
    if ( (/(^\d{11}$)/.test(address.userPhone)) ) {
      request({url:"/UpAddress",data:{address}});
      wx.showToast({
        title: '修改成功',
        icon: 'success',
        mask: true
      }); 
      wx.navigateBack({
        delta: 1
      });
    }else{
      showModal({content:"请输入正确的手机号码"});
    }
  },
  getOpenid(){
    let page = this;
    wx.cloud.callFunction({
      name:'getOpenid',
      complete:res=>{
        var openid = res.result.openid;
        page.setData({
          openid
        })
      }
    })
  },
  chooseLocation(){
    var that=this;
    wx.chooseLocation({
      latitude: 0,
      success (res) {
        that.setData({
          addresscity:res.address,
          addressname:res.name
        });
        if(res.address!=""){
          that.setData({
            dingwei:true
          })
        }
      },
      fail(){}
    });
  },
  
 
})
/**正则表达式
 * 验证数字：^[0-9]*$ 
   验证n位的数字：^\d{n}$ 
 */