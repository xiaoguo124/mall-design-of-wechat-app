// pages/pay/pay.js

import {getSetting,chooseAddress,PayLoading,showModal,showToast,PayWan,requestPayment} from "../../utils/asyncWx.js";
import regeneratorRuntime from "../../lib/runtime/runtime.js";
import {request} from "../../request/request.js";
var util =require('../../utils/util.js');

Page({
  data: {
    address:{},
    cart:[],
    totalPrice:0,
    totalNum:0,
    openid:""
  },
  onShow(){
    this.getOpenid();
    // 获取缓存中的信息 购物车
    let pages = getCurrentPages();
    let currentPage=pages[pages.length-1];
    let options=currentPage.options;
    const {addressId}=options;
   
    if(addressId!=null){
      this.SearchAddressByaddressId(addressId);
    }
    let cart=wx.getStorageSync("cart")||[];
    // 过滤购物车数组 过滤后checked都为true 数据是从缓存中获取的并不是从购物车中传过来的
    cart=cart.filter(v=>v.checked);
    
    let totalPrice=0;
    let totalNum=0;
    cart.forEach(v => {
      totalPrice+=v.num * v.goodsPrice;
      totalNum+=v.num;
    });
    this.setData({
      cart,
      totalPrice,
      totalNum
    })
    
    
  },
  async SearchAddressByaddressId(addressId){
    const res=await request({url:"/SearchAddressByaddressId",data:{addressId}});
    this.setData({
      address:res.data[0]
    })
  },
  handleChooseAddress(){
    wx.navigateTo({url: "/pages/address/address?pay=1111"});
  },
  //点击支付
  async handleOrderPay(){
    if(this.data.address.addressId==null){
      showToast({title:"您还没有选择收货地址喔"});
    }else{
      const res=await showModal({content:"你确定要支付？"});
      if(res.confirm){
        const j=await PayWan({content:"祝你购物愉快 (๑′ᴗ‵๑)Ｉ Lᵒᵛᵉᵧₒᵤ❤ "});
        if (j.confirm) {
            var TIME = util.formatTime(new Date());
            var id=util.orderId(new Date());
            let goodsId="";
            let goodsNumber="";
            if(this.data.cart.length==1){
              goodsId=this.data.cart[0].goodsId;
              goodsNumber=this.data.cart[0].num;
            }else{
              this.data.cart.forEach(i => {
                goodsId+=i.goodsId+"+";
                goodsNumber+=i.num;
              });
            };
            
            var orders={
              orderId:this.data.address.userPhone.substring(7)+id,
              orderTotalprice:this.data.totalPrice,
              userId:this.data.openid,
              consignee:this.data.address.consignee,
              addressId:this.data.address.addressId,
              goodsId:goodsId,
              goodsNumber:goodsNumber,
              createTime:TIME,
              updateTime:TIME
            };
            
            await request({url:"/AddOrders",data:{orders}});
            await showToast({title:"支付成功"});
            // 手动删除缓存中 已经支付了的商品
            let newCart=wx.getStorageSync("cart");
            newCart=newCart.filter(v=>!v.checked);
            wx.setStorageSync("cart", newCart);
            // 跳转到订单页面
            wx.navigateTo({
              url: '/pages/orders/orders'
            });
        }else{
          PayWan({content:"白送你都不要。。。"});
        }
      }
      
    }
    
  },

  getOpenid(){
    let page = this;
    wx.cloud.callFunction({
      name:'getOpenid',
      complete:res=>{
        var openid = res.result.openid;
        page.setData({
          openid
        })
      }
    })
  }
})



/**
 *页面加载 onShow()
 * 1 缓存中获取购物车数据 渲染到页面上 这些数据的 checked=true
 * 
 *微信支付
 * 可以实现微信支付的账号
 * 1 企业微信
 * 2 企业账号的小程序后台中必须给开发者添加上白名单
 *    一个appid 可以同时绑定多个开发者
 *    白名单上的开发者就可以共用这个appid和它的开发权限
 * 
 *支付按钮
 * 1 先判断缓存中有没有token
 * 2 没有 跳转授权页面
 * 3 有token 就获取
 * 4 并且创建订单 获取订单编号
 * 5 已经完成了微信支付
 * 6 手动删除缓存中 已被选中了的商品
 * 7 删除购物车数据 填充回缓存
 * 8 跳转页面
 * 
 * 
 * 
 * 在这个支付页面中 有两处获取购物车信息的地方：
 * let cart=wx.getStorageSync("cart")||[];
 * let newCart=wx.getStorageSync("cart");
 * 而且它们都需要过滤。
 * 第一个是过滤之后剩下选中的商品，也就是需要支付的，这些就是需要支付的商品
 * 第二个过滤之后剩下的是没有被选中的商品，因为购物车中被选中的已经变成了支付订单，所以不应该继续出现在购物车中
 * 
 */