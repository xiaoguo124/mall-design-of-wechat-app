// pages/category/category.js
import {request} from "../../request/request.js";
import regeneratorRuntime from "../../lib/runtime/runtime.js";

const db=wx.cloud.database();

Page({
  data: {
    GoodsType:[],
    RecommendType:[],
    WineType:[],
    CountryType:[],
    PriceType:[],
    ToolType:[]
  },
  onShow: function () {
    this.getType();
  },
  async getType(){
    const res=await request({url:"/getAllGoodsTypeForWX"});
    const res1=await request({url:"/getAllRecommendTypeForWX"});
    const res2=await request({url:"/getAllWineTypeForWX"});
    const res3=await request({url:"/getAllCountryTypeForWX"});
    const res4=await request({url:"/getAllPriceTypeForWX"});
    const res5=await request({url:"/getAllToolTypeForWX"});
    this.setData({
      GoodsType:res.data,
      RecommendType:res1.data,
      WineType:res2.data,
      CountryType:res3.data,
      PriceType:res4.data,
      ToolType:res5.data
    });
  },

})
