// pages/orders/orders.js

import {request} from "../../request/request.js";
import regeneratorRuntime from "../../lib/runtime/runtime.js";

Page({
  data: {
    openid:"",
    orders:[],
    tabs:[
      {
        id:0,
        value:"全部",
        isActive:true
      },
      {
        id:1,
        value:"待发货",
        isActive:false
      },
      {
        id:2,
        value:"待收货",
        isActive:false
      },
      {
        id:3,
        value:"退款/退货",
        isActive:false
      }
    ]
  },
  onShow(options){
    this.getOpenid();
    // 获取当前的小程序的页面栈(可看做数组) 长度最大是10个页面
    // 数组中索引最大的页面就是当前页面
    let pages=getCurrentPages();
    let currentPage=pages[pages.length-1];
    // 获取url上的type参数
    const {type}=currentPage.options;
    this.changeTitleByIndex(type-1);//id比起type在数值上小1
    this.getOrders(type);
  },
  // 获取订单列表的方法
  async getOrders(type){

  },
  handleTabsItemChange(e){
    // 获取被点击的标题索引
    const {index}=e.detail;
    this.changeTitleByIndex(index);
    // 2 重新发送请求 type=1 index=0
    this.getOrders(index+1);
  },
  // 根据标题索引来激活选中 标题数组
  changeTitleByIndex(index){
    // 修改源数组
    let { tabs } = this.data;
    tabs.forEach((v,i)=>i===index?v.isActive=true:v.isActive=false);
    this.setData({
      tabs
    })
  },
  getOpenid(){
    let page = this;
    wx.cloud.callFunction({
      name:'getOpenid',
      complete:res=>{
        var openid = res.result.openid;
        page.setData({
          openid
        });
        let userId=openid;
        wx.request({
          url: 'http://192.168.43.40/SweetWine/SearchOrdersByuserId.action',
          data: {userId},
          success: (result)=>{
            page.setData({
              orders:result.data
            });
          }
        });
      }
    })
  }
})



/**
 * 页面被打开的时候 onShow
 *  1 获取url上的参数type
 *  2 根据type发送请求 获取订单数据 并判定哪个被激活选中
 *  3 渲染页面
 * 点击不同标题 需重新发送请求来获取和渲染数据
 * 
 * onShow 不同于onLoad，无法在形参上接收options参数
 */