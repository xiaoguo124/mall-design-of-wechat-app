 
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    cateItems: [
      {
        cate_id: 1,
        cate_name: '一级酒庄',
        children: [
          {
            child_id: 1,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/111.png"
          },
          {
            child_id: 2,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/112.png"
          },
          {
            child_id: 3,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/113.png"
          },
          {
            child_id: 4,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/114.png"
          },
          
          {
            child_id: 5,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/121.png"
          },
          {
            child_id: 6,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/122.png"
          },
          {
            child_id: 7,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/123.png"
          },

          {
            child_id: 8,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/131.png"
          },
          {
            child_id: 9,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/132.png"
          },
          {
            child_id: 10,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/133.png"
          },
          {
            child_id: 11,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/134.png"
          },

          {
            child_id: 12,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/141.png"
          },
          {
            child_id: 13,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/142.png"
          },
          {
            child_id: 14,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/143.png"
          },

          {
            child_id: 15,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/151.png"
          },
          {
            child_id: 16,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/152.png"
          },
          {
            child_id: 17,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/153.png"
          },
          {
            child_id: 18,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/one/153.png"
          }
          
        ]
      },
      {
        cate_id: 2,
        cate_name: '二级酒庄',
        children: [
          {
            child_id: 1,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/211.png"
          },
          {
            child_id: 2,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/212.png"
          },
          {
            child_id: 3,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/213.png"
          },

          {
            child_id: 4,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/221.png"
          },
          {
            child_id: 5,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/222.png"
          },
          {
            child_id: 6,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/223.png"
          },

          {
            child_id: 7,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/231.png"
          },
          {
            child_id: 8,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/232.png"
          },
          {
            child_id: 9,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/233.png"
          },

          {
            child_id: 10,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/241.png"
          },
          {
            child_id: 11,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/242.png"
          },
          {
            child_id: 12,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/243.png"
          },

          {
            child_id: 13,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/251.png"
          },
          {
            child_id: 14,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/252.png"
          },
          {
            child_id: 15,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/253.png"
          },

          {
            child_id: 16,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/261.png"
          },
          {
            child_id: 17,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/262.png"
          },
          {
            child_id: 18,
            image: "https://sweetwineshop-1305549866.cos.ap-chengdu.myqcloud.com/chateau/two/263.png"
          }
        ]
      },
      {
        cate_id: 3,
        cate_name: '三级酒庄'
      },
      {
        cate_id: 4,
        cate_name: '四级酒庄'
      },
      {
        cate_id: 5,
        cate_name: '五级酒庄'
      }
      
    ],
    curNav: 1,
    curIndex: 0
  },
  //左侧导航栏索引
  switchRightTab: function (e) {
    let id = e.target.dataset.id, index = e.target.dataset.index;
    this.setData({
      curNav: id,
      curIndex: index
    })
  },
 
})