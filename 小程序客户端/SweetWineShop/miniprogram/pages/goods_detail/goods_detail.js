// pages/goods_detai/goods_detail.js

import {getSetting,chooseAddress,PayLoading,showModal,showToast,PayWan,requestPayment} from "../../utils/asyncWx.js";
import { request } from "../../request/request.js";
import regeneratorRuntime from "../../lib/runtime/runtime";
Page({
  data: {
    isCollect:false,
    login:false,
    Goods:[], //商品信息
    GoodsPics:[], //商品轮播图
    GoodsDetail:[]  //商品详细信息图片

    
  },
  GoodsInfo:{},  //商品信息，用于存在缓存
  goodspics:[],  //点击大图预览，需存放五个轮播图的路径,
  onShow: function () {
    let pages = getCurrentPages();
    let currentPage=pages[pages.length-1];
    let options=currentPage.options;
    // 这里的options 与onLoad：function(options) 里的一样效果
    // onLoad: function (options) 生命周期函数--监听页面加载
    const {goodsId}=options;
    // 以上四个只为获取 其他页面传递过来的参数 goodsId
    this.getGoodsDetail(goodsId);
    const openid=wx.getStorageSync("openid");
    if(openid!=null){
      let userId=openid;
      this.SearchUserByuserId(userId)
    }
  },
  async getGoodsDetail(goodsId){
    const res1=await request({url:"/SearchGoodsBygoodsId",data:{goodsId}});
    const res2=await request({url:"/SearchGoodsPicsBygoodsId",data:{goodsId}});
    const res3=await request({url:"/SearchGoodsDetailBygoodsId",data:{goodsId}});
    this.GoodsInfo=res1.data[0];
    let collect=wx.getStorageSync("collect")||[];
    let isCollect=collect.some(v=>v.goodsId===this.GoodsInfo.goodsId);
    this.setData({
      Goods:res1.data,
      GoodsPics:res2.data,
      GoodsDetail:res3.data,
      isCollect
    });
  },
  // 验证登录情况
  async SearchUserByuserId(userId){
    const res=await request({url:"/SearchUserByuserId",data:{userId}});
    if(res.data[0]!=null){
      if (res.data[0].haveuserinfo){
        this.setData({login:true})
      }else{
        this.setData({login:false})
      }
    }
  },
  
  // 点击轮播图 放大预览
  handlePreviewImage(e){
    const urls=this.data.GoodsPics.map(v=>v.picsUrl);
    const current=e.currentTarget.dataset.url;
    wx.previewImage({
      current,  // 当前显示图片的http链接
      urls  // 需要预览的图片http链接列表,这个是数组
    });
  },
  // 加入购物车
  handleCartAdd(){
    if(this.data.login){
      this.setCart();
      wx.showToast({
        title: '加入成功',
        icon: 'success',
        mask: true
      })
    }else{
      showToast({title:"请先登录"});
    }
  },

  // 立即购买
  handlePay(){
    if(!this.data.login){
      showToast({title:"请先登录"});
    }else{
      this.setCart();
      wx.navigateTo({url: '/pages/pay/pay'});
    }
  },

  // 商品信息压入缓存 加入购物车和立即购买都会调用，这样可以共用支付页面
  setCart(){
    let cart=wx.getStorageSync("cart")||[];
    let index=cart.findIndex(v=>v.goodsId===this.GoodsInfo.goodsId);
    if (index===-1) {
      //不存在 第一次添加
      this.GoodsInfo.num=1;
      this.GoodsInfo.checked=true;
      cart.push(this.GoodsInfo);
    } else {
      cart[index].num++;
    }
    wx.setStorageSync("cart", cart);
  },

  // 点击收藏事件
  handleCollect(){
    if(this.data.login){
      let isCollect=false;
      let collect=wx.getStorageSync("collect")||[];
      let index=collect.findIndex(v=>v.goodsId===this.GoodsInfo.goodsId);
      //不等-1说明在收藏列表中有这件商品 等于-1就说明列表中没有该商品
      if (index!==-1) {
        // index!==-1 表示已经收藏过该商品，所以点击之后应该把这个商品从收藏列表中删除
        collect.splice(index,1);
        isCollect=false;
        wx.showToast({
          title:'取消成功',
          icon:'success',
          mask:true
        });
      }else{
        // 没有收藏过的 点击之后应该是加入收藏
        collect.push(this.GoodsInfo);
        isCollect=true;
        wx.showToast({
          title:'收藏成功',
          icon:'success',
          mask:true
        });
      }
      wx.setStorageSync("collect",collect);
      this.setData({
        isCollect
      })
    }else{
      showToast({title:"请先登录"});
    }
  }
  
})



/**
 *点击轮播图 预览大图 
 *  1 给轮播图绑定点击事件
 *  2 本质是调用小程序的api previewImage
 *点击加入购物车
 *由于接口的关系 我选择小程序内置的本地存储技术来缓存购物车数据
 *  1 绑定点击事件
 *  2 获取缓存中的购物车数据 数组格式
 *  3 判断当前商品是否已经存在购物车里
 *  4 已经存在 修改商品数据 购物车数量++ 重新把购物车数组填充回缓存中
 *  5 不存在 直接给购物车数组添加一个新元素 新元素带上购买数量属性num 重新把购物车数组填充回缓存中
 *  6 弹出提示
 * 
 *商品收藏 
 * 1 页面onShow的时候 加载缓存中的商品收藏数据
 * 2 判断当前商品是否被收藏？‘改变图标’：‘’
 * 3 点击商品收藏图标
 *  判断该商品是否存在于缓存数组中
 *  已存在 把该商品删除
 *  不存在 把商品添加到收藏数组中 存入到缓存中即可
 * 
 * 
 * 
 * 
 * 
 * iphone部分手机 不识别webp图片格式
 * 最好是找后台 让他修改
 * 临时自己改 1.webp=>1.jpg
 * goods.introduce=goodsObj.goods_introduce.replace(/\.webp/g,'.jpg')
 * 
 * 
 * 
 * 
 * 
 * 
 * 用于检测数组中的元素是否满足指定条件，返回值为Boolean , 
 * 该方法是只要数组中有一项满足条件就返回true，否则false
 * let list = [1, 2, 3];
   let res = list.some(item => item > 0)
   console.log(res) // true
 */