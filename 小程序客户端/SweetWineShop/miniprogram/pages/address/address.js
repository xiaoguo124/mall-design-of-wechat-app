// pages/address/address.js

import {request} from "../../request/request.js";
import regeneratorRuntime from "../../lib/runtime/runtime.js";
var util =require('../../utils/util.js');
var wxPromise = require("../../utils/wxPromise.js");
import {getSetting,chooseAddress,openSetting,showModal,showToast} from "../../utils/asyncWx.js";


Page({
    data:{
      address:[], //存放地址信息
      openid:"",
      delBtnWidth: 180,
      payTo:false
    },
    onShow(){
      this.getOpenid();

      let pages = getCurrentPages();
      let currentPage=pages[pages.length-1];
      let options=currentPage.options;
      const {pay}=options;
      if(pay==1111){
        this.setData({
          payTo:true
        })
      }
    },
    getOpenid(){
      let page = this;
      wx.cloud.callFunction({
        name:'getOpenid',
        complete:res=>{
          var openid = res.result.openid;
          page.setData({
            openid
          });
          let userId=openid;
          wx.request({
            url: 'http://192.168.43.40/SweetWine/SearchAddressByuserId.action',
            data: {userId},
            success: (result)=>{
              page.setData({
                address:result.data
              })
            },
            fail: ()=>{},
            complete: ()=>{}
          });
        }
      })
    },
    
    // 下拉事件监听
    onPullDownRefresh(){
      this.getOpenid();
    },
    
    async DelAddress(e){
      //弹窗提示
      const res=await showModal({content:"你是否要删除？"});
      if(res.confirm){
        var addressId = e.currentTarget.dataset.id;
        request({url:"/DelAddress",data:{addressId}});
        // 删除之后 在刷新一下数据
        var userId=this.data.openid;
        const res1=await request({url:"/SearchAddressByuserId",data:{userId}});
        this.setData({
          address:res1.data
        })
      }else{
        this.getOpenid();
      }
    },

    touchS: function (e) {
      if (e.touches.length == 1) {
        this.setData({
          //设置触摸起始点水平方向位置
          startX: e.touches[0].clientX
        });
      }
    },
    touchM: function (e) {
      if (e.touches.length == 1) {
        //手指移动时水平方向位置
        var moveX = e.touches[0].clientX;
        //手指起始点位置与移动期间的差值
        var disX = this.data.startX - moveX;
        var delBtnWidth = this.data.delBtnWidth;
        var txtStyle = "";
        if (disX == 0 || disX < 0) {//如果移动距离小于等于0，文本层位置不变
          txtStyle = "left:0rpx";
        } else if (disX > 0) {//移动距离大于0，文本层left值等于手指移动距离
            txtStyle = "left:-" + disX + "rpx";
            if (disX >= delBtnWidth) {
              //控制手指移动距离最大值为删除按钮的宽度
              txtStyle = "left:-" + delBtnWidth + "rpx";
            }
          }
        //获取手指触摸的是哪一项
        var index = e.currentTarget.dataset.index;
        var list = this.data.address;
        list[index]['txtStyle'] = txtStyle;
        //更新列表的状态
        this.setData({
          address: list
        });
      }
    },
    touchE: function (e) {
      if (e.changedTouches.length == 1) {
        //手指移动结束后水平位置
        var endX = e.changedTouches[0].clientX;
        //触摸开始与结束，手指移动的距离
        var disX = this.data.startX - endX;
        var delBtnWidth = this.data.delBtnWidth;
        //如果距离小于删除按钮的1/2，不显示删除按钮
        var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "rpx" : "left:0rpx";
        //获取手指触摸的是哪一项
        var index = e.currentTarget.dataset.index;
        var list = this.data.address;
        var del_index = '';
        disX > delBtnWidth / 2 ? del_index = index : del_index = '';
        list[index].txtStyle = txtStyle;
        //更新列表的状态
        this.setData({
          address: list,
          del_index: del_index
        });
      }
    }
    
})