//const db=wx.cloud.database();
import {request} from "../../request/request.js";
import regeneratorRuntime from "../../lib/runtime/runtime.js";

Page({
  data: {
    swiperList:[],
    allGoods:[]
  },
  // 页面开始加载就会触发
  onLoad: function (options) {
    this.getSwiperList();
    this.getAllGoods();
    //this.getDatabase('swiper');
  },
  // 发送异步请求获取轮播图数据
  getSwiperList(){
    request({url: '/getAllSwiper'})
    .then(result=>{
        this.setData({
          swiperList:result.data
        })
    })
  },
  //使用es7的async发送异步请求
  //获取全部商品信息
  async getAllGoods(){
    const res=await request({url:"/getAllGoodsForWX"});
    this.setData({
      allGoods:res.data
    })
  },

  getDatabase(database){
    db.collection(database).get({
      success:(res)=>{
        this.setData({
          swiperList:res.data
        })
      },
      fail:(res)=>{}
    })
  }
})