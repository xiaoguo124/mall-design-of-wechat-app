// pages/user/user.js
import {request} from "../../request/request.js";
import regeneratorRuntime from "../../lib/runtime/runtime.js";
var util =require('../../utils/util.js');
import {getUserProfile} from "../../utils/asyncWx.js";

Page({
    data:{
      userinfo:{}, //存放用户信息
      collectNum:0,
      openid:"",
      haveuserinfo:false //记录用户的退出或登录状态
    },
    onShow(){
      const collect=wx.getStorageSync("collect")||[];
      this.setData({
        collectNum:collect.length
      });
      this.getOpenid();
    },
    getOpenid(){
      let page = this;
      wx.cloud.callFunction({
        name:'getOpenid',
        complete:res=>{
          var openid = res.result.openid;
          page.setData({
            openid
          });
          let userId=openid;
          wx.request({
            url: 'http://192.168.43.40/SweetWine/SearchUserByuserId.action',
            data: {userId},
            success: (result)=>{
              // 数据库中查询到用户信息
              if (result.data[0]!=null){
                page.setData({
                  haveuserinfo:true
                });
                // 有用户信息且为登录状态
                if(result.data[0].haveuserinfo){
                  page.setData({
                    userinfo:result.data[0]
                  })
                  wx.setStorageSync("openid", openid);
                };
              };
            },
            fail: ()=>{},
            complete: ()=>{}
          });
        }
      })
    },
    async getUserProfile(){
      // 第一步 获取先信息
      const res=await getUserProfile();
      let userInfo=res.userInfo;
      if(userInfo!=null){
        var TIME = util.formatTime(new Date());
        var user={
          userId:this.data.openid,
          openid:this.data.openid,
          nickname:userInfo.nickName,
          gender:userInfo.gender===1?'男':'女',
          avatarurl:userInfo.avatarUrl,
          country:userInfo.country,
          province:userInfo.province,
          city:userInfo.city,
          haveuserinfo:true,
          updateTime:TIME
        };
        // 第二步 用户接收授权成功之后 判断
        if (this.data.haveuserinfo) {
          // 有信息，点击授权登录，修改用户登录状态
          request({url:"/UpUser",data:{user}});
        } else {
          // 无信息，点击授权登录，向数据库添加用户
          user.createTime=TIME;
          request({url:"/AddUser",data:{user}});
        }
        this.setData({
          userinfo:user
        })
        wx.setStorageSync("openid", this.data.openid);

      }
    },
    //用户退出登录
    async Exit(){
      var TIME = util.formatTime(new Date());
      var user={
        userId:this.data.openid,
        haveuserinfo:false,
        updateTime:TIME
      };
      const res=await request({url:"/UpUser",data:{user}});
      this.setData({
        userinfo:{}
      })
    },
    // 下拉事件监听
    onPullDownRefresh(){
      this.getOpenid();
    }
    
})
/**
 * haveuserinfo  true用户之前登录过false登录但退出了 是个bool  
 * userinfo 存放数据库中查询到的用户信息 是个对象
 * 
 * 两个都是true ，显示用户界面
 * 一真二空 说明用户之前登录过 但是目前为退出状态 点授权登录视为改变用户信息
 * 两个都空 说明用户没有登录过  点授权登录视为增加用户信息
 * 没有第四种情况
 * 
 */